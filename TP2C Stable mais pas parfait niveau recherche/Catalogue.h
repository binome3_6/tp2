/*************************************************************************
                           CATALOGUE  -  description
                             -------------------
        début                : 14/11/2016
    copyright            : (C) 2016 par FALCONIERI & LACHAT
    e-mail               : vincent.falconieri@insa-lyon.fr & laetita.lachat@insa-lyon.fr
 *************************************************************************/

//---------- Interface de la classe <Catalogue> (fichier Catalogue.h) ----------------
#if ! defined ( CATALOGUE_H )
#define CATALOGUE_H

//--------------------------------------------------- Interfaces utilisées
#include "Trajet.h"
#include "TrajetCompose.h"
#include "TrajetSimple.h"
//------------------------------------------------------------- Constantes
static const int CARD_MAX_TRAJETS = 10;
static const int LONGUEUR_NOM_VILLE = 50;

//------------------------------------------------------------------ Types

//------------------------------------------------------------------------
// Rôle de la classe <Catalogue>
// Catalogue est une collection de parcours 
// est ordonnee d'objets heterogenes (trajets simples et trajets simples)
//
//------------------------------------------------------------------------

class Catalogue 
{
    //----------------------------------------------------------------- PUBLIC

public:
    //----------------------------------------------------- Méthodes publiques
    // type Méthode ( liste de paramètres );
    // Mode d'emploi :
    //
    // Contrat :
    //

    void Afficher();
    // Mode d'emploi : Affiche tous les trajets possibles contenus dans catalogue
    //
    // Contrat : aucun
    //
    
    void Rechercher();
    // Mode d'emploi : Recherche d'un parcours
    //
    // Contrat : aucun
    //
    
    void Menu();
    // Mode d'emploi : Affiche le menu pour gérer le catalogue
    //
    // Contrat : aucun
    //

    void RechercherSimple(const char * villeA, const char * villeB);
    // Mode d'emploi : Recherche d'un parcours avec un algorithme simple (non exhaustif)
    //
    // Contrat : aucun

    void RechercherComplexe(const char * villeA, const char * villeB);
    // Mode d'emploi : Recherche d'un parcours avec un algorithme long (exhaustif)
    //
    // Contrat : aucun
    
    bool Ajouter(); 
    // Mode d'emploi : Ajoute un trajet au catalogue en demandant la saisie.
    //
    // Contrat : aucun
    //

    //------------------------------------------------- Surcharge d'opérateurs
    //Catalogue & operator=(const Catalogue & unCatalogue);
    // Mode d'emploi :
    //
    // Contrat : aucun
    //


    //-------------------------------------------- Constructeurs - destructeur
    Catalogue(const Catalogue & unCatalogue);
    // Mode d'emploi (constructeur de copie) :
    // Permet de fournir une copie du catalogue donné en paramètre.
    // Contrat : aucun
    //

    Catalogue();
    // Mode d'emploi : 
    // Constructeur de catalogue. Créé un catalogue vide.
    // 
    //

    virtual ~Catalogue();
    // Mode d'emploi :
    // Permet de détruire un catalogue
    // Contrat : aucun
    //

    //------------------------------------------------------------------ PRIVE

private:
    //------------------------------------------------------- Méthodes privées
    unsigned int Ajuster(int delta);
    static moyenDeTransport saisieTransport();
    static int saisieChoixMenu();
    bool RechercherInt(int* ListeInt, int tailleListe, int valeurATrouver);
    int RechercherRecursive(int indiceDebut, const char * villeB, int depth, Trajet ** ListeParcoursTMP, int* ListeParcoursIndices, int nbParcoursTrouves);
    char* Normalise (char* motNonNormalise); 

protected:
    //----------------------------------------------------- Attributs protégés
    int nbParcours;
    int nbParcoursMax;
    Trajet ** Collection;

    //------------------------------------------------------- Attributs privés

};

//---------------------------------------------- Types dépendants de <Catalogue>

#endif // CATALOGUE_H

