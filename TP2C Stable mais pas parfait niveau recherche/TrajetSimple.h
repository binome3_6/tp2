/*************************************************************************
                           TrajetSimple  -  description
                             -------------------
    début                : 14/11/2016
    copyright            : (C) 2016 par FALCONIERI & LACHAT
    e-mail               : vincent.falconieri@insa-lyon.fr & laetita.lachat@insa-lyon.fr
 *************************************************************************/

//---------- Interface de la classe <TrajetSimple> (fichier TrajetSimple.h) ----------------
#if ! defined ( TrajetSimple_H )
#define TrajetSimple_H

//--------------------------------------------------- Interfaces utilisées
#include "Trajet.h"
//------------------------------------------------------------- Constantes
static const int nbMoyenTransport = 6;
static const char* moyenDeTransportChaine[] = {"voiture", "train", "bateau", "avion", "marche", "vélo"};
//------------------------------------------------------------------ Types
typedef enum {
    VOITURE, TRAIN, BATEAU, AVION, MARCHE, VELO
} moyenDeTransport;

//------------------------------------------------------------------------
// Rôle de la classe <TrajetSimple>
// Un trajet simple gère un trajet "unaire", un déplacement élémentaire entre 
// deux villes, effectué grâce à un moyen de transport spécifié. 
//
//------------------------------------------------------------------------

class TrajetSimple : public Trajet 
{
    //----------------------------------------------------------------- PUBLIC

public:
    //----------------------------------------------------- Méthodes publiques
    // type Méthode ( liste de paramètres );

    virtual const char * getVilleA();
    // Mode d'emploi : Affiche la ville de depart
    //
    // Contrat : aucun
    //

    virtual const char * getVilleB();
    // Mode d'emploi : Affiche la ville de destination
    //
    // Contrat : aucun
    //

    virtual const moyenDeTransport getMoyen();
    // Mode d'emploi : Affiche le moyen de transport
    //
    // Contrat : aucun
    //

    virtual void Afficher();
    // Mode d'emploi : Affiche le trajet (villeDepart, villeDestination, MoyenTransport)
    //
    // Contrat : aucun
    //

    //------------------------------------------------- Surcharge d'opérateurs
    TrajetSimple & operator=(const TrajetSimple & unTrajetSimple);
    // Déclaré et non initialisé pour éviter l'utilisation de l'opérateur par défaut
    //


    //-------------------------------------------- Constructeurs - destructeur
    TrajetSimple(const TrajetSimple & unTrajetSimple);
    // Mode d'emploi (constructeur de copie) : FOurnit copie du TrajetSimple
    //
    // Contrat : aucun
    //

    TrajetSimple(const char * villeDepart, const char * villeArrivee, const moyenDeTransport transport);
    // Mode d'emploi : 
    // Declare villeDepart comme villeA; villeArrivee comme villeB, transport comme moyenTransport
    // Contrat : aucun
    //

    virtual ~TrajetSimple();
    // Mode d'emploi :
    //
    // Contrat : aucun
    //

    //------------------------------------------------------------------ PRIVE

protected:
    //----------------------------------------------------- Méthodes protégées

private:
    //------------------------------------------------------- Méthodes privées
    static void AfficherEnumMoyen(moyenDeTransport ceMoyen);

protected:
    //----------------------------------------------------- Attributs protégés
    char * villeDepart;
    char * villeDestination;
    moyenDeTransport moyenTransport;

private:
    //------------------------------------------------------- Attributs privés

};

//---------------------------------------------- Types dépendants de <TrajetSimple>

#endif // TrajetSimple_H

