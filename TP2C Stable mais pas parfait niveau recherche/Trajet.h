/*************************************************************************
                           TRAJET  -  description
                             -------------------
    début                : 14/11/2016
    copyright            : (C) 2016 par FALCONIERI & LACHAT
    e-mail               : vincent.falconieri@insa-lyon.fr & laetita.lachat@insa-lyon.fr
 *************************************************************************/

//---------- Interface de la classe <TRAJET> (fichier Trajet.h) ----------------
#if ! defined ( TRAJET_H )
#define TRAJET_H

//--------------------------------------------------- Interfaces utilisées

//------------------------------------------------------------- Constantes

//------------------------------------------------------------------ Types

//------------------------------------------------------------------------
// Rôle de la classe <Trajet>
// La classe trajet représente le parent des classes "Trajet Simple" et "Trajet composé"
// Elle permet d'utiliser le polymorphisme pour les méthodes de classes communes aux deux
// entités citées précedemment. 
//------------------------------------------------------------------------

class Trajet 
{
    //----------------------------------------------------------------- PUBLIC

public:
    //----------------------------------------------------- Méthodes publiques
    virtual const char * getVilleA();
    // Mode d'emploi :
    // Permet de récupérer la ville de départ d'un trajet donné.
    // Contrat :
    // aucun

    virtual const char * getVilleB();
    // Mode d'emploi :
    // Permet de récupérer la ville d'arrivée d'un trajet donné.
    // Contrat :
    // aucun

    virtual void Afficher();
    // Mode d'emploi :
    // Permet de fournir un affichage du trajet sur la sortie standard.
    // du format : "de A à B en MT1"
    // Contrat :
    // aucun

    //------------------------------------------------- Surcharge d'opérateurs
    Trajet & operator=(const Trajet & unTrajet);
    // Déclaré et non initialisé pour éviter l'utilisation de l'opérateur par défaut
    //


    //-------------------------------------------- Constructeurs - destructeur
    Trajet(const Trajet & unTrajet);
    // Mode d'emploi (constructeur de copie) :
    // Permet de fournir une copie d'un trajet donné en paramètre.
    // Est pisté.
    // Contrat :
    //

    Trajet();
    // Mode d'emploi :
    // Permet d'instancier un trajet.
    // Est pisté.
    // Contrat :
    //

    virtual ~Trajet();
    // Mode d'emploi :
    // Permet de détruire un trajet.
    // Est pisté.
    // Contrat :
    //

    //------------------------------------------------------------------ PRIVE

protected:
    //----------------------------------------------------- Méthodes protégées

private:
    //------------------------------------------------------- Méthodes privées

protected:
    //----------------------------------------------------- Attributs protégés

private:
    //------------------------------------------------------- Attributs privés

};

//---------------------------------------------- Types dépendants de <Trajet>

#endif // TRAJET_H