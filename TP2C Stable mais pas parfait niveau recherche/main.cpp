/*************************************************************************
                           MAIN  -  description
                             -------------------
        début                : 14/11/2016
    copyright            : (C) 2016 par FALCONIERI & LACHAT
    e-mail               : vincent.falconieri@insa-lyon.fr & laetita.lachat@insa-lyon.fr
 *************************************************************************/

//---------- Réalisation du module <MAIN> (fichier MAIN.cpp) ---------------

/////////////////////////////////////////////////////////////////  INCLUDE
//-------------------------------------------------------- Include système
#include <iostream>
#include <cstring>
using namespace std;
//------------------------------------------------------ Include personnel
#include "Catalogue.h"
#include "Trajet.h"
#include "TrajetSimple.h"
#include "TrajetCompose.h"
//////////////////////////////////////////////////////////////////  PRIVE
//------------------------------------------------------------- Constantes

//------------------------------------------------------------------ Types

//---------------------------------------------------- Variables statiques

//------------------------------------------------------ Fonctions privées
//static type nom ( liste de paramètres )
// Mode d'emploi :
//
// Contrat :
//
// Algorithme :
//
//{
//} //----- fin de nom

static void testMonTrajetSimple()
{

    cout << " == DEBUT testMonTrajetSimple == " << endl;

    char * nomVilleA = new char[10];
    char * nomVilleB = new char[10];

    strcpy(nomVilleA, "Lyon");
    strcpy(nomVilleB, "Bordeaux");
    moyenDeTransport monTransport = TRAIN;

    //Test l'initialisation d'un trajet simple
    cout << " == Test l'initialisation d'un trajet simple == " << endl;
    TrajetSimple monTrajet = TrajetSimple(nomVilleA, nomVilleB, monTransport);

    cout << " == Test des fonctions de base d'un trajet simple == " << endl;
    //Test des fonctions de base d'un trajet simple
    monTrajet.getVilleA();
    cout << endl;

    monTrajet.getVilleB();
    cout << endl;

    monTrajet.getMoyen();
    cout << endl;

    monTrajet.Afficher();
    cout << endl;

    cout << " == Test de la copie de trajets simples == " << endl;
    TrajetSimple monTrajet1 = TrajetSimple(nomVilleA, nomVilleB, monTransport);
    TrajetSimple monTrajet2 = TrajetSimple(monTrajet1);

    cout << "Le trajet 1 est : " << endl;
    monTrajet1.Afficher();
    cout << endl;
    cout << "Le trajet 2 est : " << endl;
    monTrajet2.Afficher();
    cout << endl;
    cout << "Ils doivent être identiques" << endl;

    cout << " == Nettoyage == " << endl;
    delete[] nomVilleA;
    delete[] nomVilleB;

    cout << " == FIN testMonTrajetSimple == " << endl;

}

static void testMonTrajetCompose()
{
    cout << " == DEBUT testMonTrajetCompose == " << endl;

    char * nomVilleA = new char[10];
    char * nomVilleB = new char[10];
    char * nomVilleC = new char[10];
    char * nomVilleD = new char[10];
    char * nomVilleE = new char[10];
    char * nomVilleF = new char[10];
    char * nomVilleG = new char[10];

    strcpy(nomVilleA, "Lyon");
    strcpy(nomVilleB, "Bordeaux");
    strcpy(nomVilleC, "Toulouse");
    strcpy(nomVilleD, "Nice");
    strcpy(nomVilleE, "Marseille");
    strcpy(nomVilleF, "Lille");
    strcpy(nomVilleG, "Brest");

    moyenDeTransport monTransport1 = VOITURE;
    moyenDeTransport monTransport2 = BATEAU;
    moyenDeTransport monTransport3 = MARCHE;

    //Test l'initialisation d'un trajet composé
    cout << " == Test l'initialisation d'un trajet composé == " << endl;
    TrajetCompose monTrajet = TrajetCompose();

    cout << " == Test des fonctions de base d'un trajet composé à vide == " << endl;
    //Test des fonctions de base d'un trajet composé
    monTrajet.getVilleA();
    cout << endl;

    monTrajet.getVilleB();
    cout << endl;

    monTrajet.Afficher();
    cout << endl;

    cout << " == Test l'ajout de 1 trajet == " << endl;
    monTrajet.AjouterTrajetSimple(nomVilleA, nomVilleB, monTransport1);

    cout << " == Test des fonctions de base d'un trajet composé avec 1 trajet == " << endl;
    //Test des fonctions de base d'un trajet composé
    monTrajet.getVilleA();
    cout << endl;

    monTrajet.getVilleB();
    cout << endl;

    monTrajet.Afficher();
    cout << endl;

    cout << " == Test l'ajout de n trajets == " << endl;
    monTrajet.AjouterTrajetSimple(nomVilleB, nomVilleC, monTransport1);
    cout << endl;
    monTrajet.AjouterTrajetSimple(nomVilleC, nomVilleD, monTransport2);
    cout << endl;
    monTrajet.AjouterTrajetSimple(nomVilleD, nomVilleE, monTransport3);
    cout << endl;
    monTrajet.AjouterTrajetSimple(nomVilleE, nomVilleF, monTransport1);
    cout << endl;
    monTrajet.AjouterTrajetSimple(nomVilleF, nomVilleG, monTransport2);
    cout << endl;
    monTrajet.AjouterTrajetSimple(nomVilleG, nomVilleE, monTransport2);
    cout << endl;
    // PERMET DE TESTER L'AJUSTEMENT AUTOMATIQUE
    monTrajet.AjouterTrajetSimple(nomVilleE, nomVilleG, monTransport2);
    cout << endl;
    monTrajet.AjouterTrajetSimple(nomVilleG, nomVilleE, monTransport2);
    cout << endl;
    monTrajet.AjouterTrajetSimple(nomVilleE, nomVilleG, monTransport2);
    cout << endl;
    monTrajet.AjouterTrajetSimple(nomVilleG, nomVilleE, monTransport2);
    cout << endl;
    monTrajet.AjouterTrajetSimple(nomVilleE, nomVilleD, monTransport2);
    cout << endl;
    //Doit générer une erreur

    cout << " == Test des fonctions de base d'un trajet composé avec n trajets == " << endl;
    //Test des fonctions de base d'un trajet composé
    monTrajet.getVilleA();
    cout << endl;

    monTrajet.getVilleB();
    cout << endl;

    monTrajet.Afficher();
    cout << endl;

    cout << " == Nettoyage == " << endl;
    delete[] nomVilleA;
    delete[] nomVilleB;
    delete[] nomVilleC;
    delete[] nomVilleD;
    delete[] nomVilleE;
    delete[] nomVilleF;
    delete[] nomVilleG;

    cout << " == FIN testMonTrajetCompose == " << endl;
}

static void testMonCatalogue()
{
    cout << " == DEBUT testMonCatalogue == " << endl;

    //Test l'initialisation d'un monCatalogue
    cout << " == Test l'initialisation d'un Catalogue == " << endl;
    Catalogue monCatalogue = Catalogue();

    cout << " == Test des fonctions de base d'un Catalogue == " << endl;
    //Test des fonctions de base d'un monCatalogue
    monCatalogue.Afficher();
    cout << endl;

    cout << " == Test l'ajout de 1 ou n trajet == " << endl;

    monCatalogue.Menu();
    cout << endl;

    monCatalogue.Afficher();
    cout << endl;

    cout << " == Test des fonctions de base d'un trajet composé avec n trajets == " << endl;
    char * nomVilleA = new char[10];
    char * nomVilleB = new char[10];
    strcpy(nomVilleA, "Lyon");
    strcpy(nomVilleB, "Bordeaux");

    monCatalogue.Afficher();
    cout << endl;

    monCatalogue.RechercherSimple(nomVilleA, nomVilleB);
    cout << endl;

    cout << " == Nettoyage == " << endl;
    delete[] nomVilleA;
    delete[] nomVilleB;

    cout << " == FIN testMonCatalogue == " << endl;
}

static void testCompletManuel()
{
    cout << " == DEBUT de la fonction MAIN == " << endl;

    cout << " == Initialisation d'un Catalogue == " << endl;
    Catalogue monCatalogue = Catalogue();
    monCatalogue.Menu();
    cout << endl;

    cout << " == FIN de la fonction MAIN == " << endl;
}
//////////////////////////////////////////////////////////////////  PUBLIC
//---------------------------------------------------- Fonctions publiques

int main()
// Algorithme :
//
{
    cout << "Debut fonction MAIN " << endl;
    //testMonTrajetSimple();
    //testMonTrajetCompose();
    //testMonCatalogue();
    testCompletManuel();

    cout << "Fin fonction MAIN " << endl;

    return 0;

} //----- fin de main

