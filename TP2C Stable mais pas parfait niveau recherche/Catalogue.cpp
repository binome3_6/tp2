/*************************************************************************
                           CATALOGUE  -  description
                             -------------------
    début                : 14/11/2016
    copyright            : (C) 2016 par FALCONIERI & LACHAT
    e-mail               : vincent.falconieri@insa-lyon.fr & laetita.lachat@insa-lyon.fr
 *************************************************************************/

//---------- Réalisation de la classe <Catalogue> (fichier Catalogue.cpp) ------------

//---------------------------------------------------------------- INCLUDE

//-------------------------------------------------------- Include système
using namespace std;
#include <iostream>
#include <cstring>

//------------------------------------------------------ Include personnel
#include "Catalogue.h"
#include "Trajet.h"

//------------------------------------------------------------ Constantes

//------------------------------------------------------------- Constantes

//----------------------------------------------------------------- PUBLIC

//----------------------------------------------------- Méthodes publiques
// type Catalogue::Méthode ( liste des paramètres )
// Algorithme :
//
//{
//} //----- Fin de Méthode

void Catalogue::Afficher()
{

#ifdef MAP
    cout << "CATALOGUE : méthode 'Affichage' d'un Parcours appelée " << endl;
#endif
    if (nbParcours == 0)
    {
        cout << "Aucun parcours enregistré";
    }
    else
    {
        cout << "> Les parcours stockés dans le catalogue sont :" << endl;
        for (int i = 0; i < nbParcours; i++)
        {
#ifdef MAP
            cout << "CATALOGUE : méthode 'Affichage'- Affichage du N°" << i << endl;
#endif
            cout << "Parcours N°" << i+1 << " : ";
            Collection[i]->Afficher();
            cout << endl;
        }
        cout << "> Fin des parcours stockés" << endl;
    }

}//----- Fin de Afficher

void Catalogue::Rechercher()
{
#ifdef MAP
    cout << "CATALOGUE : méthode 'Rechercher' - Lancement de la fonction de recherche " << endl;
#endif
    cout << " == RECHERCHE == " << endl;
    cout << "> Instructions : " << endl;
    cout << "> Veuillez saisir le trajet que vous voulez rechercher." << endl;

    cout << "> Quelle est votre ville de départ ?" << endl;
    char villeDepart [LONGUEUR_NOM_VILLE];
    //cin >> villeDepart; // Voir explications plus bas
    cin.ignore();
    cin.getline(villeDepart, LONGUEUR_NOM_VILLE);
    cout << endl;
    
    cout << "> Quelle est votre ville d'arrivée ? " << endl;
    char villeArrivee [LONGUEUR_NOM_VILLE];
    //cin >> villeArrivee; // Voir explications plus bas
    cin.getline(villeArrivee, LONGUEUR_NOM_VILLE);
    cout << endl;

#ifdef MAPV
    cout << "CATALOGUE : méthode 'Rechercher' - Lancement de la recherche avec : " << villeDepart << " " << villeArrivee << endl;
#endif

    //On effectue la recherche
    cout << "Résultat(s) de la recherche : " << endl;
    //this->RechercherSimple(Normalise(villeDepart), Normalise(villeArrivee));
    this->RechercherComplexe(Normalise(villeDepart), Normalise(villeArrivee));

#ifdef MAPV
    cout << "CATALOGUE : méthode 'Rechercher' - Nettoyage du Catalogue de réponse" << endl;
#endif

    // On veut donc quitter le programme
    cout << endl << "== RECHERCHE TERMINEE == " << endl;

}//----- Fin de Rechercher

void Catalogue::RechercherSimple(const char * villeA, const char * villeB)
// On fait une simple vérification en parcours tous les trajets de la collection.
// Si leur trajet d'arrivée et de départ matchent avec les paramètres passés à la fonction
// On demande son affichage.
{
#ifdef MAP
    cout << "CATALOGUE : méthode 'RechercherSimple' - Lancement de la recherche simple" << endl;
#endif

    bool auMoinsUneReponse = false;

    for (int i = 0; i < nbParcours; i++)
    {
        if (!strcmp(Collection[i]->getVilleA(), villeA) && !strcmp(Collection[i]->getVilleB(), villeB))
        {
#ifdef MAP
            cout << "CATALOGUE : méthode 'RechercherSimple' - Affichage de l'élément" << i << endl;
#endif
            auMoinsUneReponse = true;
            cout << "Parcours N°" << i << " : ";
            Collection[i]->Afficher();
        }
    }

    if (!auMoinsUneReponse)
    {
        cout << "Aucun parcours ne correspond à votre recherche." << endl;
    }
#ifdef MAP
    cout << "CATALOGUE : méthode 'RechercherSimple' - Fin de l'affichage" << endl;
#endif
}//----- Fin de RechercherSimple

void Catalogue::RechercherComplexe(const char * villeA, const char * villeB)
{
    // On déclare un pointeur temporaire
    Trajet ** ListeParcoursTMP;
    // stockage du chemin pendant l'exploration recursive
    int* ListeParcoursIndices;

    ListeParcoursTMP = new Trajet*[this->nbParcours];
    ListeParcoursIndices = new int[this->nbParcours];

    int nbResultat = 0;

    for (int i = 0; i < this->nbParcours; i++)
    {
#ifdef MAP
        cout << "CATALOGUE : méthode 'Rechercher Complexe' - élément " << i << " analysé" << endl;
        cout << "CATALOGUE : méthode 'Rechercher Complexe' - nbResultats " << nbResultat << endl;
#endif
        //Si la villeA match avec la ville A de l'élément i de la liste
        if (!strcmp(Collection[i]->getVilleA(), villeA))
        {
            nbResultat += RechercherRecursive(i, villeB, 0, ListeParcoursTMP, ListeParcoursIndices, nbResultat);
        }
    }

#ifdef MAP
    cout << "CATALOGUE : méthode 'Rechercher Complexe' - nbResultats final " << nbResultat << endl;
#endif

    if (nbResultat == 0)
    {
        cout << "Aucun parcours ne correspond à votre recherche." << endl;
    }

    // Finalement on nettoie
    delete[] ListeParcoursTMP;
    delete[] ListeParcoursIndices;
    // Note : on ne nettoie pas DANS les Liste pour ne pas taper sur les vraies valeurs !
    // Ce n'est que de la copie en surface !

}//----- Fin de RechercherComplexe

void Catalogue::Menu()
{
#ifdef MAP
    cout << "CATALOGUE : méthode 'Menu(2)' d'un Catalogue appelée " << endl;
#endif

    //On appelle la fonction d'affichage et de sélection du choix
    int choix = saisieChoixMenu();

    while (choix != 0)
    {
#ifdef MAP
        cout << "CATALOGUE : méthode 'Menu(2)' - choix effectué" << choix << endl;
#endif
        bool answer = false;

        switch (choix)
        {
        case 1:
            // L'utilisateur veut faire un ajout.
            answer = this->Ajouter();
            cout << "> Votre ajout d'un nouveau parcours " << (answer ? "s'est bien déroulé." : "a été annulé.") << endl;
            cout << "== Fin de la fonction d'ajout ==" << endl;
            break;
        case 2:
            // L'utilisateur veut faire un recherche.
            this->Rechercher();
            cout << "== Fin de la fonction de recherche ==" << endl;
            break;
        case 3:
            // L'utilisateur veut faire un affichage.
            this->Afficher();
            cout << "== Fin de la fonction d'affichage ==" << endl;
            break;
        default:
            // Aucune idée de ce qu'il se passe
            cout << "Erreur : votre saisie est incorrecte." << endl;
            break;
        }

        //On repart pour un tour
        choix = saisieChoixMenu();
    }
    // On veut donc quitter le programme
    cout << "> Votre saisie est terminée. " << endl;
    cout << "== Merci d'avoir utilisé notre catalogue == " << endl;
}//----- Fin de MenuDe

bool Catalogue::Ajouter()
{
    bool answer = false;

#ifdef MAP
    cout << "CATALOGUE : méthode 'Ajouter(2)' d'un Catalogue appelée " << endl;
#endif

    cout << " == AJOUT DE TRAJET(s) == " << endl;

    cout << "> Quelle est votre ville de départ ?" << endl;
    char villeDepart [LONGUEUR_NOM_VILLE];
    //cin >> villeDepart; // Voir explications plus bas
    cin.ignore(); //Difficile de justifier .. mais c'est ainsi que cin ne "mange" pas la première lettre.
    cin.getline(villeDepart, LONGUEUR_NOM_VILLE);
    cout << endl;

    cout << "> Quelle est votre ville d'arrivée ? " << endl;
    char villeArrivee [LONGUEUR_NOM_VILLE];
    //cin >> villeArrivee; // Voir explications plus bas
    cin.getline(villeArrivee, LONGUEUR_NOM_VILLE);
    cout << endl;

    cout << "> Si vous désirez terminer votre saisie, tapez 0. " << endl;
    cout << "> Si vous désirez continuer votre saisie, tapez 1. " << endl;
    int quit;
    cin >> quit;
    cout << endl;

    if (quit == 0) //Donc si "0", on veut terminer la saisie
    {
        //On récupère le moyen de transport
        moyenDeTransport transport = saisieTransport();

        // ---- on procède à l'ajout -----
        //Si on a plus de place, on ajuste.
        if (nbParcours == nbParcoursMax)
        {
            this->Ajuster(nbParcoursMax);
        }
        // On envoit les paramètres normalisés au constructeur d'un trajet simple
        Collection[nbParcours] = new TrajetSimple(Normalise(villeDepart), Normalise(villeArrivee), transport);

        //On valide l'ajout à l'écran
        nbParcours++;
        cout << "> Votre nouveau trajet a été correctement ajouté." << endl;

        answer = true;

#ifdef MAP
        cout << "CATALOGUE : méthode 'Ajouter(2)' - le parcours ajouté est N° : " << nbParcours - 1 << endl;
        Collection[nbParcours - 1]->Afficher();
#endif
    }
    else //Donc si tout sauf "0", on veut continuer la saisie
    {
        //On créé un pointeur sur notre trajetComposé courant.
        TrajetCompose* trajetComposeCourant;

        // ---- on procède à l'ajout -----
        //Si on a plus de place, on ajuste.
        if (nbParcours == nbParcoursMax)
        {
            this->Ajuster(nbParcoursMax);
        }

        //On créé un nouveau trajet composé, sur lequel on récupère un pointeur
        Collection[nbParcours] = new TrajetCompose();
        trajetComposeCourant = (TrajetCompose*) (&(*Collection[nbParcours]));

        while (quit != 0)
        {
            cout << "> Quelle est votre ville d'étape ?" << endl;
            char villeTMP [LONGUEUR_NOM_VILLE];

            // La première solution est celle ci :
            //cin >> villeTMP;
            // Mais dans ce cas, la ville "La boulay" ou "Grand Bornand" est invalide puisque cin coupe à l'espace !

            //La seconde solution est donc :
            cin.ignore();
            cin.getline(villeTMP, LONGUEUR_NOM_VILLE);

#ifdef MAP
            cout << "CATALOGUE : méthode 'Ajouter(2)' - la ville capture est: " << villeTMP << endl;
#endif

            //On récupère le moyen de transport
            moyenDeTransport transport = saisieTransport();

            // On ajoute le trajetsimple au trajet composé.
            trajetComposeCourant->AjouterTrajetSimple(Normalise(villeDepart), Normalise(villeTMP), transport);

#ifdef MAP
            cout << "CATALOGUE : méthode 'Ajouter(2)' - le parcours ajouté est N° : " << nbParcours << endl;
            Collection[nbParcours]->Afficher();
#endif

            //On valide l'ajout à l'écran
            cout << "> Votre nouveau trajet a été correctement ajouté." << endl;
            answer = true;

            // On fait une rotation : notre ville temporaire actuele devient la villeDépart pour le prochaine étape.
            strcpy(villeDepart, villeTMP);

            //On gère la fin de boucle
            cout << "> Si vous désirez terminer votre saisie, tapez 0 : " << endl;
            cout << "> Si vous désirez continuer votre saisie, tapez 1. " << endl;
            cin >> quit;
            cout << endl;
        }

        // On a quitté la boucle de création des étapes intermédiaires.
        //On récupère le moyen de transport
        moyenDeTransport transport = saisieTransport();

        // On clos donc le cycle par l'ajout final :
        trajetComposeCourant->AjouterTrajetSimple(Normalise(villeDepart), Normalise(villeArrivee), transport);

        //On valide l'ajout à l'écran
        nbParcours++;
        cout << "> Votre dernier trajet a été correctement ajouté." << endl;
    }

#ifdef MAP
    cout << "CATALOGUE : méthode 'Ajouter(2)' - le parcours ajouté était le N° : " << nbParcours - 1 << endl;
    Collection[nbParcours - 1]->Afficher();
#endif

    return answer;
} //----- Fin de Ajouter

//------------------------------------------------- Surcharge d'opérateurs

//-------------------------------------------- Constructeurs - destructeur

Catalogue::Catalogue(const Catalogue & unCatalogue)
{
#ifdef MAP
    cout << "Appel au constructeur de copie de <Catalogue>" << endl;
#endif
} //----- Fin de Catalogue (constructeur de copie)

Catalogue::Catalogue()
{
    nbParcours = 0;
    nbParcoursMax = CARD_MAX_TRAJETS;

    // On déclare un pointeur temporaire
    Trajet ** CollectionTMP;

    // On instancie ce pointeur sur un tableau de pointeurs de Trajet contenant CARD_MAX_TRAJETS éléments
    CollectionTMP = new Trajet*[CARD_MAX_TRAJETS];

    //On fait pointer le vrai pointeur sur la même chose.
    Collection = CollectionTMP;

    // NOTE : à cet instant, on a un tableau de pointeurs, non instancié, qui pointent n'importe où.

#ifdef MAP
    cout << "Appel au constructeur de <Catalogue>" << endl;
#endif
} //----- Fin de Catalogue

Catalogue::~Catalogue()
// Algorithme :
//
{
#ifdef MAP
    cout << "Appel au destructeur de <Catalogue>" << endl;
#endif

#ifdef MAPV
    cout << "CATALOGUE : méthode 'Destructeur' - Lancement du delete de chaque élément du tableau " << endl;
#endif
    for (int i = 0; i < nbParcours; i++)
    {
        delete Collection[i];
#ifdef MAPV
        cout << "CATALOGUE : méthode 'Destructeur' - delete l'élément " << i << endl;
#endif
    }

#ifdef MAPV
    cout << "CATALOGUE : méthode 'Destructeur' - Delete du tableau entier" << endl;
#endif
    delete[] Collection;

} //----- Fin de ~Catalogue


//------------------------------------------------------------------ PRIVE

//----------------------------------------------------- Méthodes protégées

//------------------------------------------------------- Méthodes privées

unsigned int Catalogue::Ajuster(int delta)
// Algorithme :
// Si delta>0 : on agrandit la structure de delta case
// Si delta=0 : on garde la structure telle quelle
// Si delta<0 : on réduit la structure de delta ou à défaut au maximum.
// On renvoit la taille finale du tableau.
{
    unsigned int answer = nbParcoursMax;

#ifdef MAP
    cout << "CATALOGUE:  : méthode 'Ajuster' appelée avec delta= " << delta << endl;
#endif

    if (delta > 0)
    {
        Trajet ** CollectionTMP = new Trajet*[nbParcoursMax + delta];
        for (int i = 0; i < nbParcours; i++)
        {
            CollectionTMP[i] = Collection[i];
        }

        delete[] Collection;

        Collection = CollectionTMP;

        nbParcoursMax = nbParcoursMax + delta;
        answer = nbParcoursMax;
#ifdef MAP
        cout << "CATALOGUE:  : méthode 'Ajuster' - Après ajout (delta >0) nbParcoursMax : " << nbParcoursMax << endl;
#endif
    }
    else if (delta < 0)
    {
        // Si il y a assez de place pour réduire
        if (-delta <= nbParcoursMax - nbParcours) // si ça passe
        {
            nbParcoursMax = nbParcoursMax + delta; // car delta < 0
            answer = nbParcoursMax;
#ifdef MAP
            cout << "CATALOGUE:  : méthode 'Ajuster' - Après réduction (delta<0) nbParcoursMax : " << nbParcoursMax << endl;
#endif
        }
        else // si il reste de la place, mais qu'on veut en supprimer trop, on reset le max à actuel
        {
            nbParcoursMax = nbParcours;
            answer = nbParcoursMax;
        }
    }
#ifdef MAP
    cout << "CATALOGUE: méthode 'Ajuster' - valeur retournée finale : " << nbParcoursMax << endl;
#endif

    return answer;
}//----- Fin de Ajuster

int Catalogue::RechercherRecursive(int indiceDebut, const char * villeB, int depth, Trajet ** ListeParcoursTMP, int* ListeParcoursIndices, int nbParcoursTrouves)
{

    //On fait comme si le Parcours courant faisait partie du résultat.
    ListeParcoursTMP[depth] = Collection[indiceDebut];
    ListeParcoursIndices[depth] = indiceDebut;

    // on est sur le sommet d'arrivé -> fini
    if (!strcmp(ListeParcoursTMP[depth]->getVilleB(), villeB))
    {
#ifdef MAP
        cout << "CATALOGUE : méthode 'explore' - élément - on a fini ! Affichage : " << endl;
#endif
        // affiche la solution
        for (int i = 0; i <= depth; i++)
        {
            cout << "Parcours N°" << nbParcoursTrouves << " : ";
            ListeParcoursTMP[i]->Afficher();
            if (i != 0 && i != depth)
            {
                cout << " - ";
            }
        }
        // On a trouvé un parcours
        nbParcoursTrouves++;
#ifdef MAPV
        cout << "CATALOGUE : méthode 'Rechercher Complexe' - On retourne " << nbParcoursTrouves << endl;
#endif
        return nbParcoursTrouves;
    }

    // sinon...

    // on a déjà posé un caillou au début de la fonction

    // on explore les chemins restants
    for (int i = 0; i < this->nbParcours; i++)
    {
        // Si la ville B courante est la ville A de l'étape suivante, et qu'on est pas déjà passé par le parcours en quesiton
        if (!strcmp(Collection[i]->getVilleA(), ListeParcoursTMP[depth]->getVilleB()) && !RechercherInt(ListeParcoursIndices, depth, i))
        {
            // On explore un cran de plus, avec :
            // i = le nouveau point de départ, villeB = tjr identique, le même point d'arrivée
            // depth+1 = on est à "depth+1" trajets déjà rangés, ListesPacours qui servent de transmission du chemin actuel
            nbParcoursTrouves += RechercherRecursive(i, villeB, depth + 1, ListeParcoursTMP, ListeParcoursIndices, nbParcoursTrouves);
        }
    }

    // Soit on a trouvé tous les chemins qui pouvaient utiliser ce parcours, soit on a rien trouvé.
    // Dans tous les cas, on le retire de la liste courante.
    ListeParcoursTMP[depth] = NULL;
    ListeParcoursIndices[depth] = -1;
    // = on retire le caillou
#ifdef MAPV
    cout << "CATALOGUE : méthode 'Rechercher Complexe' - On retourne " << nbParcoursTrouves << endl;
#endif
    return nbParcoursTrouves;
}//----- Fin de RechercherRecursive

bool Catalogue::RechercherInt(int* ListeInt, int tailleListe, int valeurATrouver)
{
    bool answer = false;

    for (int i = 0; i < tailleListe; i++)
    {
        if (ListeInt[i] == valeurATrouver)
        {
            answer = true;
        }
    }

    return answer;
}//----- Fin de RechercherInt

char* Catalogue::Normalise(char* motNonNormalise)
{
#ifdef MAP
    cout << "CATALOGUE - Appel de la methode Normalise" << endl;
#endif

    //parcours de tous les caracteres composant motNonNormalise
    for (unsigned int i = 0; i < strlen(motNonNormalise); i++)
    {
        char lettre = motNonNormalise[i];
        int codeASCIIlettre = (int) lettre; //traduction en code ASCII decimal
        if (codeASCIIlettre <= 122 && codeASCIIlettre >= 97 && i == 0) //caractere correspond a une minuscule et en debut du mot
        {
            codeASCIIlettre = codeASCIIlettre - 32; //debient une majuscule
#ifdef MAPV
            cout << "Code ascii premiere lettre modifiee" << endl;
#endif
        }
        if (codeASCIIlettre >= 65 && codeASCIIlettre <= 90 && i > 0) //caractere correspond a une majuscule
        {
            codeASCIIlettre = codeASCIIlettre + 32; //devient une minuscule
#ifdef MAPV
            cout << "Code ascii " << i + 1 << "eme lettre modifiee" << endl;
#endif
        }
        motNonNormalise[i] = (char) codeASCIIlettre;
    }
#ifdef MAP
    cout << "CATALOGUE - Appel de la methode Normalise finie" << endl;
    cout << "CATALOGUE - Appel de la methode Normalise - valeur rendue finale : " << motNonNormalise << endl;
#endif
    return motNonNormalise;
}

moyenDeTransport Catalogue::saisieTransport()
{
#ifdef MAP
    cout << "CATALOGUE : méthode 'saisieTransport' d'un Catalogue appelée " << endl;
#endif

    cout << "> Liste des moyens de transport : " << endl;
    // On récupère et on affiche la liste des moyens de transport disponibles
    for (int i = 0; i < nbMoyenTransport; i++)
    {
        cout << i << " : " << moyenDeTransportChaine[i] << endl;
    }

    //On récupère le choix utilisateur.
    cout << "> Quelle est votre moyen de transport pour cette étape ?" << endl;
    int numeroMoyen;
    cin >> numeroMoyen;

    //Normalement, les deux tableaux correspondent, on peut donc récupérer le moyen de transport comme ceci :
    moyenDeTransport transport = static_cast<moyenDeTransport> (numeroMoyen);

    return transport;
}//----- Fin de saisieTransport

int Catalogue::saisieChoixMenu()
{
#ifdef MAP
    cout << "CATALOGUE : méthode 'saisieChoixMenu' d'un Catalogue appelée " << endl;
#endif
    cout << "== Gestion de catalogue ==" << endl;

    cout << "> Instructions : " << endl;
    cout << "> Saisissez '1' pour ajouter un trajet. " << endl;
    cout << "> Saisissez '2' pour effectuer une recherche. " << endl;
    cout << "> Saisissez '3' pour afficher le catalogue. " << endl;
    cout << "> Saisissez '0' pour terminer votre saisie. " << endl;
    cout << "> Votre saisie : " << endl;

    // On récupère le choix utilisateur
    int choix;
    cin >> choix;

    return choix;
}//----- Fin de saisieChoixMenu