/*************************************************************************
                           TrajetCompose  -  description
                             -------------------
    début                : 14/11/2016
    copyright            : (C) 2016 par FALCONIERI & LACHAT
    e-mail               : vincent.falconieri@insa-lyon.fr & laetita.lachat@insa-lyon.fr
 *************************************************************************/

//---------- Réalisation de la classe <TrajetCompose> (fichier TrajetCompose.cpp) ------------

//---------------------------------------------------------------- INCLUDE

//-------------------------------------------------------- Include système
using namespace std;
#include <iostream>
#include <cstring>

//------------------------------------------------------ Include personnel
#include "TrajetCompose.h"
#include "TrajetSimple.h"

//------------------------------------------------------------- Constantes

//---------------------------------------------------- Variables de classe

//----------------------------------------------------------- Types privés


//----------------------------------------------------------------- PUBLIC
//-------------------------------------------------------- Fonctions amies

//----------------------------------------------------- Méthodes publiques

const char * TrajetCompose::getVilleA()
// Algorithme : On récupère la ville du trajet de la première case du tableau.
// Note : récursivement, si la première case comporte un trajet composé, cela fonctionne.
{
    const char* answer = nullptr;
#ifdef MAP
    cout << "TRAJET COMPOSE : méthode 'getVilleA' d'un TRAJET Composé appelée " << endl;
#endif
    if (nbTrajets == 0)
    {
        answer = villeVide;
#ifdef MAPV
        cout << "TRAJET COMPOSE - méthode 'getVilleA' - answer : " << answer << endl;
#endif
    }
    else
    {
        // Answer, un pointeur, prendra la valeur de l'adresse qui pointe sur l'objet "VilleA"
        // si doute sur l'écriture : voir la copie de l'explication en annexe.
        answer = &*listeTrajets[0]->getVilleA();

#ifdef MAPV
        cout << "== DEBUG DE 'answer = &*listeTrajets[0]->getVilleA()' ==" << endl;
        cout << " answer " << answer << endl;
        //cout << " *listeTrajets[0]->getVilleA() " << *listeTrajets[0]->getVilleA() << endl;
        cout << " &*listeTrajets[0]->getVilleA() " << &*listeTrajets[0]->getVilleA() << endl;
#endif
    }

    return answer;
}//----- Fin de Méthode

const char * TrajetCompose::getVilleB()
// Algorithme : On récupère la ville du trajet de la dernière case remplie du tableau.
// Note : récusrivement, si la dernière case comporte un trajet composé, cela fonctionne.
{
    const char * answer = nullptr;

#ifdef MAP
    cout << "TRAJET COMPOSE : méthode 'getVilleB' d'un TRAJET Composé appelée " << endl;
#endif

    if (nbTrajets == 0)
    {
        answer = villeVide;
#ifdef MAPV
        cout << "TRAJET COMPOSE - méthode 'getVilleB' - answer : " << answer << endl;
#endif
    }
    else
    {
        //On doit décaler avec le "-1" pour revenir à l'indice 0 si il y a un seul objet
        // Answer, un pointeur, prendra la valeur de l'adresse qui pointe sur l'objet "VilleB"
        // si doute sur l'écriture : voir la copie de l'explication en annexe.
        answer = &*listeTrajets[nbTrajets - 1]->getVilleB();
#ifdef MAP
        cout << "TRAJET COMPOSE - méthode 'getVilleB' - Accès au N° : " << nbTrajets - 1 << endl;
#endif
#ifdef MAPV
        cout << "== DEBUG DE 'answer = &*listeTrajets[nbTrajets - 1]->getVilleB()' ==" << endl;
        cout << " answer " << answer << endl;
        //cout << " *listeTrajets[nbTrajets-1]->getVilleB() " << *listeTrajets[nbTrajets-1]->getVilleB() << endl;
        cout << " &*listeTrajets[nbTrajets-1]->getVilleB() " << &*listeTrajets[nbTrajets - 1]->getVilleB() << endl;
#endif
    }

    return answer;
}//----- Fin de Méthode

void TrajetCompose::Afficher()
// Algorithme : On appelle l'affichage de chaque case du tableau remplie, en appellant
// la fonction afficher de chaque Trajet.
// Note : récursivement, si une case du tableau comporte un trajet composé, cela fonctionne encore !
{
#ifdef MAP
    cout << "TRAJET COMPOSE : méthode 'Affichage' d'un TRAJET Composé appelée " << endl;
#endif
    for (int i = 0; i < nbTrajets; i++)
    {
#ifdef MAP
        cout << "TRAJET COMPOSE : méthode 'Affichage'- Affichage du N°" << i << endl;
#endif
        listeTrajets[i]->Afficher();
        if (i != nbTrajets - 1)
        {
            cout << " - ";
        }
    }
    cout << endl;
}//----- Fin de Méthode

Trajet* TrajetCompose::getCopy() const {
    TrajetCompose * answer = new TrajetCompose();

    //On ajoute tous les trajets
    for (int i = 0; i < this->nbTrajets; i++)
    {
        Trajet* trajetComposantTemporaire = this->listeTrajets[i]->getCopy();
        // On créé le nouveau trajet
        answer->AjouterTrajet(*trajetComposantTemporaire);
        delete trajetComposantTemporaire;
    }

    return answer;
}

bool TrajetCompose::AjouterTrajetSimple(char * villeDepart, char * villeArrivee, moyenDeTransport transport)
// Algorithme :
//
{
#ifdef MAP
    cout << "TRAJET COMPOSE : méthode 'AjouterTrajetSimple' appelée " << endl;
#endif
    bool answer = false;

    //Si il y a assez de place
    if (nbTrajets < nbMaxTrajets)
    {
#ifdef MAPV
        cout << "TRAJET COMPOSE : méthode 'AjouterTrajetSimple' - il reste de la place " << endl;
#endif
        if (nbTrajets == 0 || !strcmp(listeTrajets[nbTrajets - 1]->getVilleB(), villeDepart))
        {
            //Si la ville B du précédent est égal à la ville A de l'ajouté OU qu'on ajoute la première ville

            listeTrajets[nbTrajets] = new TrajetSimple(villeDepart, villeArrivee, transport);
            // On instancie au bout du pointeur de la case (n) un objet "TrajetSimple"

#ifdef MAP
            cout << "TRAJET COMPOSE : méthode 'AjouterTrajetSimple' - un trajet simple a été ajouté " << endl;
#endif
            nbTrajets++;
            answer = true;
        }
        else
        {
            cout << "Erreur : La ville d'arrivée du précédent trajet n'est pas la ville de départ du trajet tentant d'être ajouté." << endl;
#ifdef MAP
            cout << "TRAJET COMPOSE - méthode 'AjouterTrajetSimple' - ville précédente : " << listeTrajets[nbTrajets - 1]->getVilleB() << endl;
#endif
        }
    }
    else
    {
#ifdef MAPV
        cout << "TRAJET COMPOSE : méthode 'AjouterTrajetSimple' - il n'y a plus de place, on doit ajuster " << endl;
#endif
        //Sinon, il faut réajuster. Ici on double la taille à chaque fois.
        this->Ajuster(nbMaxTrajets);
        //De nouveau on tente l'ajoût
        this->AjouterTrajetSimple(villeDepart, villeArrivee, transport);
    }

    return answer;
}//----- Fin de Méthode

bool TrajetCompose::AjouterTrajet(Trajet const unTrajet)
// Algorithme :
//
{
    bool answer = false;

    //Si il y a assez de place
    if (nbTrajets < nbMaxTrajets)
    {
#ifdef MAP
        cout << "TRAJET COMPOSE : méthode 'AjouterTrajet' - il reste de la place " << endl;
#endif
        // On récupère une copie du trajet désirant être copié.
        Trajet* TrajetTMP = unTrajet.getCopy();
TrajetTMP->Afficher();
        
#ifdef MAPV
        cout << "TRAJET COMPOSE : méthode 'AjouterTrajet' - le trajet ajouté est : " << TrajetTMP->Afficher() << endl;
#endif

        // On place le pointeur de cette copie directement la liste.
        listeTrajets[nbTrajets] = TrajetTMP;
        // On instancie au bout du pointeur de la case (n) un objet Trajet tel que celui passé en paramètre

#ifdef MAP
        cout << "TRAJET COMPOSE : méthode 'AjouterTrajet' - un trajet simple a été ajouté " << endl;
#endif

        nbTrajets++;
        answer = true;
    }
    else
    {
#ifdef MAPV
        cout << "TRAJET COMPOSE : méthode 'AjouterTrajet' - il n'y a plus de place, on doit ajuster " << endl;
#endif
        //Sinon, il faut réajuster. Ici on double la taille à chaque fois.
        this->Ajuster(nbMaxTrajets);
        //De nouveau on tente l'ajoût
        this->AjouterTrajet(unTrajet);
    }

    return answer;
}//----- Fin de Méthode

// type TrajetCompose::Méthode ( liste de paramètres )
// Algorithme :
//
//{
//} //----- Fin de Méthode


//------------------------------------------------- Surcharge d'opérateurs
/*
TrajetCompose & TrajetCompose::operator=(const TrajetCompose & unTrajetCompose)
// Algorithme :
//
{
} //----- Fin de operator =
 */

//-------------------------------------------- Constructeurs - destructeur

TrajetCompose::TrajetCompose(const TrajetCompose & unTrajetCompose)
// Algorithme :
//
{
    this->nbTrajets = 0; // car on va le remplir ensuite, ce qui ajustera la valeur.
    this->nbMaxTrajets = unTrajetCompose.nbMaxTrajets;

    // On déclare un pointeur temporaire
    Trajet ** listeTrajetsTMP;
    // On instancie ce pointeur sur un tableau de pointeurs de Trajet contenant nbMaxTrajets éléments
    listeTrajetsTMP = new Trajet*[unTrajetCompose.nbMaxTrajets];

    //On fait pointer le vrai pointeur sur la même chose.
    this->listeTrajets = listeTrajetsTMP;

    //On ajoute tous les trajets
    for (int i = 0; i < unTrajetCompose.nbTrajets; i++)
    {
        // On créé le nouveau trajet

        // On fait un getCopie sur l'objet "Trajet" courant.
        // this->AjouterTrajet(unTrajetCompose.listeTrajets[i]->getCopy());

        Trajet* monTrajetSimpleTMP = unTrajetCompose.listeTrajets[i]->getCopy();
        // On ne fait que l'ajouter
        this->AjouterTrajet(*monTrajetSimpleTMP);
        delete monTrajetSimpleTMP;
    }

#ifdef MAP
    cout << "Appel au constructeur de copie de <TrajetCompose>" << endl;
#endif
} //----- Fin de TrajetCompose (constructeur de copie)

TrajetCompose::TrajetCompose() : Trajet() {

    nbTrajets = 0;
    nbMaxTrajets = CARD_MAX;

    // On déclare un pointeur temporaire
    Trajet ** listeTrajetsTMP;
    // On instancie ce pointeur sur un tableau de pointeurs de Trajet contenant CARD_MAX éléments
    listeTrajetsTMP = new Trajet*[CARD_MAX];

    //On fait pointer le vrai pointeur sur la même chose.
    listeTrajets = listeTrajetsTMP;

    // NOTE : à cet instant, on a un tableau de pointeurs, non instancié, qui pointent n'importe où.

#ifdef MAP
    cout << "Appel au constructeur de <TrajetCompose>" << endl;
#endif
} //----- Fin de TrajetCompose

TrajetCompose::~TrajetCompose()
// Algorithme :
//
{
#ifdef MAP
    cout << "Appel au destructeur de <TrajetCompose>" << endl;
#endif

#ifdef MAPV
    cout << "TRAJET COMPOSE : méthode 'Destructeur' - Lancement du delete de chaque élément du tableau " << endl;
#endif
    for (int i = 0; i < nbTrajets; i++)
    {
        delete listeTrajets[i];
#ifdef MAPV
        cout << "TRAJET COMPOSE : méthode 'Destructeur' - delete l'élément " << i << endl;
#endif
    }

#ifdef MAPV
    cout << "TRAJET COMPOSE : méthode 'Destructeur' - Delete du tableau entier" << endl;
#endif
    delete[] listeTrajets;

} //----- Fin de ~TrajetCompose


//------------------------------------------------------------------ PRIVE

//----------------------------------------------------- Méthodes protégées

//------------------------------------------------------- Méthodes privées

unsigned int TrajetCompose::Ajuster(int delta)
// Algorithme : 
// Si delta>0 : on agrandit la structure de delta case
// Si delta=0 : on garde la structure telle quelle
// Si delta<0 : on réduit la structure de delta ou à défaut au maximum.
// On renvoit la taille finale du tableau.
{
    unsigned int answer = nbMaxTrajets;
#ifdef MAP
    cout << "TRAJET COMPOSE : méthode 'Ajuster' appelée avec delta= " << delta << endl;
#endif

    if (delta > 0)
    {
        Trajet ** listeTrajetsTMP = new Trajet*[nbMaxTrajets + delta];
        for (int i = 0; i < nbTrajets; i++)
        {
            listeTrajetsTMP[i] = listeTrajets[i];
        }

        delete[] listeTrajets;

        listeTrajets = listeTrajetsTMP;

        nbMaxTrajets = nbMaxTrajets + delta;
        answer = nbMaxTrajets;
#ifdef MAP
        cout << "TRAJET COMPOSE : méthode 'Ajuster' - Après ajout (delta >0) nbMaxTrajets : " << nbMaxTrajets << endl;
#endif
    }
    else if (delta < 0)
    {
        // Si il y a assez de place pour réduire
        if (-delta <= nbMaxTrajets - nbTrajets) // si ça passe
        {
            nbMaxTrajets = nbMaxTrajets + delta; // car delta < 0
            answer = nbMaxTrajets;
#ifdef MAP
            cout << "TRAJET COMPOSE : méthode 'Ajuster' - Après réduction (delta<0) nbMaxTrajets : " << nbMaxTrajets << endl;
#endif
        }
        else // si il reste de la place, mais qu'on veut en supprimer trop, on reset le max à actuel
        {
            nbMaxTrajets = nbTrajets;
            answer = nbMaxTrajets;
        }
    }
#ifdef MAP
    cout << "TRAJET COMPOSE : méthode 'Ajuster' - valeur retournée finale : " << nbMaxTrajets << endl;
#endif

    return answer;
}
