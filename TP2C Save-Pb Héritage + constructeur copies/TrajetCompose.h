/*************************************************************************
                           TRAJET  -  description
                             -------------------
    début                : 14/11/2016
    copyright            : (C) 2016 par FALCONIERI & LACHAT
    e-mail               : vincent.falconieri@insa-lyon.fr & laetita.lachat@insa-lyon.fr
 *************************************************************************/

//---------- Interface de la classe <TrajetCompose> (fichier TrajetCompose.h) ----------------
#if ! defined ( TRAJETCOMPOSE_H )
#define TRAJETCOMPOSE_H

//--------------------------------------------------- Interfaces utilisées
#include "Trajet.h"
#include "TrajetSimple.h"
//------------------------------------------------------------- Constantes
static const int CARD_MAX = 10;
static const char* villeVide = (char*) "vide"; // L'ISO C++ s'attend à voir un string. En castant, on résoud.
//------------------------------------------------------------------ Types

//------------------------------------------------------------------------
// Rôle de la classe <TrajetCompose>
//
//
//------------------------------------------------------------------------

class TrajetCompose : public Trajet {
    //----------------------------------------------------------------- PUBLIC

public:
    //----------------------------------------------------- Méthodes publiques
    virtual const char * getVilleA();
    // Mode d'emploi :
    // Permet de récupérer la ville de départ du trajet composé total.
    // Contrat :
    // aucun

    virtual const char * getVilleB();
    // Mode d'emploi :
    // Permet de récupérer la ville d'arrivée du trajet composé total.
    // Contrat :
    // aucun

    virtual void Afficher();
    // Mode d'emploi :
    // Permet de fournir un affichage de l'ensemble des trajets compris dans le 
    // trajet composé sur la sortie standard.
    // du format : "de A à B en MT1 - de Y à Z en MT2"
    // Contrat :
    // aucun

    virtual Trajet* getCopy() const;
    // Mode d'emploi :
    // Fournie une copie de l'objet trajet courant
    // Contrat :
    // aucun

    bool AjouterTrajetSimple(char * villeDepart, char * villeArrivee, moyenDeTransport transport);
    // Mode d'emploi : On lui donne en paramètre les composants d'un trajet simple
    // qui sera ajouté à la liste des trajets composant le trajet composé courant.
    //
    // Contrat :
    //

    bool AjouterTrajet(Trajet const unTrajet);
    // Mode d'emploi : On lui donne en paramètre un trajet qui sera stocké dans la liste
    // des trajets composants le trajet composé (dans l'ordre d'arrivé des trajets)
    // La ville de départ de ce trajet doit être la même que la ville d'arrivée précédente.
    // Note : on peut donc faire des structures récursives en lui donnant un TrajetComposé
    // Contrat :
    // IL FAUT QUE LE TRAJET SOIT CREE AVANT ! On ne fait pas la copie DANS la fonction ! 
    // ar problème d'héritage multiple : on ne peut pas appeller le bon constructeur de copie,
    // Puisque par défaut, on appelle le constructeur de copie "Trajet" qui est vide ! 


    //------------------------------------------------- Surcharge d'opérateurs
    //TrajetCompose & operator=(const TrajetCompose & unTrajetCompose);
    // Mode d'emploi :
    //
    // Contrat :
    //


    //-------------------------------------------- Constructeurs - destructeur
    TrajetCompose(const TrajetCompose & unTrajetCompose);
    // Mode d'emploi (constructeur de copie) :
    //
    // Contrat :
    //

    TrajetCompose();
    // Mode d'emploi :
    //
    // Contrat :
    //

    virtual ~TrajetCompose();
    // Mode d'emploi :
    //
    // Contrat :
    //

    //------------------------------------------------------------------ PRIVE

protected:
    //----------------------------------------------------- Méthodes protégées

private:
    //------------------------------------------------------- Méthodes privées
    unsigned int Ajuster(int delta);

protected:
    //----------------------------------------------------- Attributs protégés
    int nbTrajets;
    int nbMaxTrajets;
    Trajet ** listeTrajets;

private:
    //------------------------------------------------------- Attributs privés

    //---------------------------------------------------------- Classes amies

    //-------------------------------------------------------- Classes privées

    //----------------------------------------------------------- Types privés

};

//---------------------------------------------- Types dépendants de <TrajetCompose>

#endif // TRAJETCOMPOSE_H

