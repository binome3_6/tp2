/*************************************************************************
                           CATALOGUE  -  description
                             -------------------
    début                : 14/11/2016
    copyright            : (C) 2016 par FALCONIERI & LACHAT
    e-mail               : vincent.falconieri@insa-lyon.fr & laetita.lachat@insa-lyon.fr
 *************************************************************************/

//---------- Réalisation de la classe <Catalogue> (fichier Catalogue.cpp) ------------

//---------------------------------------------------------------- INCLUDE

//-------------------------------------------------------- Include système
using namespace std;
#include <iostream>
#include <cstring>

//------------------------------------------------------ Include personnel
#include "Catalogue.h"
#include "Trajet.h"

//------------------------------------------------------------ Constantes

//------------------------------------------------------------- Constantes

//----------------------------------------------------------------- PUBLIC

//----------------------------------------------------- Méthodes publiques
// type Catalogue::Méthode ( liste des paramètres )
// Algorithme :
//
//{
//} //----- Fin de Méthode

void Catalogue::Afficher() {

#ifdef MAP
    cout << "CATALOGUE : méthode 'Affichage' d'un Parcours appelée " << endl;
#endif
    for (int i = 0; i < nbParcours; i++)
    {
#ifdef MAP
        cout << "CATALOGUE : méthode 'Affichage'- Affichage du N°" << i << endl;
#endif
        Collection[i]->Afficher();
    }
    cout << endl;

}

void Catalogue::Rechercher() {
#ifdef MAP
    cout << "CATALOGUE : méthode 'Rechercher' - Lancement de la fonction de recherche " << endl;
#endif
    cout << " == RECHERCHE == " << endl;
    cout << "> Instructions : " << endl;
    cout << "> Veuillez saisir le trajet que vous voulez rechercher, sur ce modèle : " << endl;
    cout << "de VilleA à VilleB" << endl;
    cout << "> Votre saisie : " << endl;

    // On récupère la première ligne
    char* tampon = new char[80];
    cin.getline(tampon, 80);

    //On génère notre tableaux de pointeurs, pour gérer les arguments
    char **arguments = Parser(tampon, NB_MAX_ARGUMENTS_RECHERCHE);

    int nb_arguments_effectifs = CompteurArguments(arguments, NB_MAX_ARGUMENTS_RECHERCHE);

    switch (nb_arguments_effectifs)
    {
        case 0:
            // L'utilisateur a rentré n'importe quoi.
            cout << "Erreur : Votre saisie ne comporte aucun arguments." << endl;
            break;
        case NB_MAX_ARGUMENTS_RECHERCHE:
            // L'utilisateur a rentré n'importe quoi.
            cout << "Erreur : Votre saisie comporte trop d'arguments." << endl;
            break;
        case NB_MAX_ARGUMENTS_RECHERCHE - 1:
            //On effectue la recherche
            cout << "Résultat(s) de la recherche : " << endl;
            //Catalogue* catalogueReponse = 
            this->RechercherSimple(arguments[1], arguments[3]);
#ifdef MAPV
            cout << "CATALOGUE : méthode 'Rechercher' - Nettoyage du Catalogue de réponse" << endl;
#endif
            //delete catalogueReponse;
            break;

            // PROBLEME AVEC CE DEFAULT ? 
            /*default :
                // Aucune idée de ce qu'il se passe
                cout << "Erreur : votre saisie est incorrecte" << endl;
                break;*/
    }

    // On veut donc quitter le programme
    cout << " == RECHERCHE TERMINEE == " << endl;

    //On nettoie finalement
    delete[] tampon;
    Nettoyeur(arguments, NB_MAX_ARGUMENTS_RECHERCHE);
}

Catalogue* Catalogue::RechercherSimple(const char * villeA, const char * villeB)
// On fait une simple vérification en parcours tous les trajets de la collection.
// Si leur trajet d'arrivée et de départ matchent avec les paramètres passés à la fonction
// On demande son affichage.
{
#ifdef MAP
    cout << "CATALOGUE : méthode 'RechercherSimple' - Lancement de la recherche simple" << endl;
#endif

    bool auMoinsUneReponse = false;

    for (int i = 0; i < nbParcours; i++)
    {
        if (!strcmp(Collection[i]->getVilleA(), villeA) && !strcmp(Collection[i]->getVilleB(), villeB))
        {
#ifdef MAP
            cout << "CATALOGUE : méthode 'RechercherSimple' - Affichage de l'élément" << i << endl;
#endif
            auMoinsUneReponse = true;
            Collection[i]->Afficher();
        }
    }

    if (!auMoinsUneReponse)
    {
        cout << "Aucun parcours ne correspond à votre recherche." << endl;
    }
#ifdef MAP
    cout << "CATALOGUE : méthode 'RechercherSimple' - Fin de l'affichage" << endl;
#endif
}

Catalogue* Catalogue::RechercherComplexe(const char * villeA, const char * villeB) {
#ifdef MAP
    cout << "CATALOGUE : méthode 'Rechercher Complexe' - Lancement de la recherche complexe" << endl;
#endif

}

bool Catalogue::Ajouter(Trajet nvTrajet) {
    bool answer = false;
    return answer;
}

void Catalogue::Menu() {

#ifdef MAP
    cout << "CATALOGUE : méthode 'Menu' d'un Catalogue appelée " << endl;
#endif

    cout << "== Gestion de catalogue ==" << endl;

    cout << "> Instructions : " << endl;
    cout << "> Saisissez 'N' pour ajouter un trajet. " << endl;
    cout << "> Saisissez 'R' pour effectuer une recherche. " << endl;
    cout << "> Saisissez 'A' pour afficher le catalogue. " << endl;
    cout << "> Saisissez 'q' pour terminer votre saisie. " << endl;
    cout << "> Votre saisie : " << endl;

    // On récupère la première ligne
    char* tampon = new char[80];
    cin.getline(tampon, 80);

    //On génère notre tableaux de pointeurs, pour gérer les arguments
    char **arguments = Parser(tampon, NB_MAX_ARGUMENTS_MENU);

    while (strcmp(arguments[0], "q"))
    {
        // On compte le nombre d'arguments de la saisie courante
        int nb_arguments_effectifs = CompteurArguments(arguments, NB_MAX_ARGUMENTS_MENU);

        switch (nb_arguments_effectifs)
        {
            case 0:
                // L'utilisateur a rentré n'importe quoi.
                cout << "Erreur : Votre saisie ne comporte aucun arguments." << endl;
                break;
            case 1:
                // La première lettre du premier mot est un "N" ou "n" = nouveau trajet
                if (arguments[0][0] == 'N' || arguments[0][0] == 'n')
                {
                    // L'utilisateur veut faire un ajout.
                    bool answer = this->Ajouter();
                    cout << "Votre ajout d'un nouveau parcours " << (answer ? "s'est bien déroulé." : "a été annulé.") << endl;
                    cout << "== Fin de la fonction d'ajout ==";
                }

                // La première lettre du premier mot est "R" ou "r" = recherche
                if (arguments[0][0] == 'R' || arguments[0][0] == 'r')
                {
                    // L'utilisateur veut faire un recherche.
                    this->Rechercher();
                    cout << "== Fin de la fonction de recherche ==";
                }

                // La première lettre du premier mot est "R" ou "r" = recherche
                if (arguments[0][0] == 'A' || arguments[0][0] == 'a')
                {
                    // L'utilisateur veut faire un recherche.
                    this->Afficher();
                    cout << "== Fin de la fonction d'affichage ==";
                }
                break;
            case NB_MAX_ARGUMENTS_MENU:
                // L'utilisateur a rentré n'importe quoi.
                cout << "Erreur : Votre saisie comporte trop d'arguments." << endl;
                break;
            default:
                // Aucune idée de ce qu'il se passe
                cout << "Erreur : votre saisie est incorrecte" << endl;
                break;
        }

        //On repart pour un tour
        cout << "> Instructions : " << endl;
        cout << "> Saisissez 'N' pour ajouter un trajet. " << endl;
        cout << "> Saisissez 'R' pour effectuer une recherche. " << endl;
        cout << "> Saisissez 'A' pour afficher le catalogue. " << endl;
        cout << "> Saisissez 'q' pour terminer votre saisie. " << endl;
        cout << "> Votre saisie : " << endl;

        //On nettoie
        Nettoyeur(arguments, NB_MAX_ARGUMENTS_MENU);
        // On reprendre une ligne
        cin.getline(tampon, 80);
        arguments = Parser(tampon, NB_MAX_ARGUMENTS_MENU);
    }
    // On veut donc quitter le programme
    cout << "> Votre saisie est terminée. " << endl;
    cout << "== Merci d'avoir utilisé notre catalogue == " << endl;

    //On nettoie finalement
    delete[] tampon;
    Nettoyeur(arguments, NB_MAX_ARGUMENTS_MENU);

}

bool Catalogue::Ajouter() {
    bool answer = false;

#ifdef MAP
    cout << "CATALOGUE : méthode 'Ajouter' d'un Catalogue appelée " << endl;
#endif

    cout << " == AJOUT DE TRAJET(s) == " << endl;

    cout << "> Instructions : " << endl;
    cout << "> Veuillez saisir le trajet que vous voulez ajouter, sur ce modèle : " << endl;
    cout << "de VilleA à villeB en MoyenTransport \nde VilleB à villeC en MoyenTransport" << endl;
    cout << "> Saisissez 'q' pour terminer votre saisie. " << endl;
    cout << "> Votre saisie : " << endl;

    // On récupère la première ligne
    char* tampon = new char[80];
    cin.getline(tampon, 80);

    //On génère notre tableaux de pointeurs, pour gérer les arguments
    char **arguments = Parser(tampon, NB_MAX_ARGUMENTS_AJOUT);
    bool estPremierAjout = true;
    TrajetCompose* trajetComposeCourant = NULL;

    while (strcmp(arguments[0], "q"))
    {
        // On compte le nombre d'arguments de la saisie courante
        int nb_arguments_effectifs = CompteurArguments(arguments, NB_MAX_ARGUMENTS_AJOUT);

        switch (nb_arguments_effectifs)
        {
            case 0:
                // L'utilisateur a rentré n'importe quoi.
                cout << "Erreur : Votre saisie ne comporte aucun arguments." << endl;
                break;
            case NB_MAX_ARGUMENTS_AJOUT:
                // L'utilisateur a rentré n'importe quoi.
                cout << "Erreur : Votre saisie comporte trop d'arguments." << endl;
                break;
            case NB_MAX_ARGUMENTS_AJOUT - 1:
                //Si on a plus de place, on ajuste.
                if (nbParcours == nbParcoursMax)
                {
                    this->Ajuster(nbParcoursMax);
                }
                //Si c'est le premier tour de boucle.
                if (estPremierAjout)
                {
                    //On créé un TrajetCompose de plus.
                    Collection[nbParcours] = new TrajetCompose();

                    //Astuce pour pouvoir bien gérer cela comme un TrajetCompose
                    trajetComposeCourant = (TrajetCompose*) (&(*Collection[nbParcours]));
                    // NOTE : on caste la Collection[NbParcours] comme un TrajetComposé avant de copier le pointeur.

                    //On ajoute le premier trajet simple
                    bool ajoutPremierTrajetSimple = false;

                    if (estMoyenTransportValide(arguments[5]))
                    {
                        moyenDeTransport moyenCourant = chaineToMoyenTransport(arguments[5]);
                        ajoutPremierTrajetSimple = trajetComposeCourant->AjouterTrajetSimple(arguments[1], arguments[3], moyenCourant);
                    }
                    else
                    {
                        cout << "Erreur : le moyen de transport spécifié n'est pas valide" << endl;
                        //On nettoie
                        delete Collection[nbParcours];
                    }

                    //Si tout s'est bien passé, on considère que le premier ajout a été effecuté, qu'on a un parcours en plus.
                    if (ajoutPremierTrajetSimple)
                    {
                        estPremierAjout = false;
                        nbParcours++;
                        cout << "Votre nouveau trajet a été correctement ajouté." << endl;
                        answer = true;
#ifdef MAP
                        cout << "CATALOGUE : méthode 'Ajouter' - le parcours ajouté est N° : " << nbParcours - 1 << endl;
                        Collection[nbParcours - 1]->Afficher();
#endif
                    }
                    else
                    {
                        cout << "Erreur : impossible de créer un nouveau parcours." << endl;
                    }
                }
                else
                {
                    //On tente d'ajouter un trajet simple supplémentaire
                    bool ajoutPremierTrajetSimple = false;

                    //Si le moyen de transport courant est correct
                    if (estMoyenTransportValide(arguments[5]))
                    {
                        moyenDeTransport moyenCourant = chaineToMoyenTransport(arguments[5]);
                        ajoutPremierTrajetSimple = trajetComposeCourant->AjouterTrajetSimple(arguments[1], arguments[3], moyenCourant);
                    }
                    else
                    {
                        cout << "Erreur : le moyen de transport spécifié n'est pas valide" << endl;
                    }

                    //Si tout s'est bien passé, on considère que le premier ajout a été effecuté, qu'on a un trajet simple en plus.
                    if (ajoutPremierTrajetSimple)
                    {
                        cout << "Votre trajet a été ajouté à ce parcours." << endl;
                        answer = true;
#ifdef MAP
                        cout << "CATALOGUE : méthode 'Ajouter' - le parcours ajouté est N° : " << nbParcours - 1 << endl;
                        Collection[nbParcours - 1]->Afficher();
#endif
                    }
                    else
                    {
                        cout << "Erreur : impossible d'ajouter un nouveau trajet à ce parcours" << endl;
                    }
                }
                break;
            default:
                // Aucune idée de ce qu'il se passe
                cout << "Erreur : votre saisie est incorrecte" << endl;
                break;
        }

        cout << "> Votre saisie : " << endl;
        //On nettoie
        Nettoyeur(arguments, NB_MAX_ARGUMENTS_AJOUT);
        // On reprendre une ligne
        cin.getline(tampon, 80);
        arguments = Parser(tampon, NB_MAX_ARGUMENTS_AJOUT);
    }
    // On veut donc quitter le programme
    cout << "> Votre saisie est terminée. " << endl;

    //On nettoie finalement
#ifdef MAPV
    cout << "CATALOGUE : méthode 'Ajouter' - Nettoyage du tampon" << endl;
#endif
    delete[] tampon;
#ifdef MAPV
    cout << "CATALOGUE : méthode 'Ajouter' - Nettoyage du tableau d'arguments" << endl;
#endif
    Nettoyeur(arguments, NB_MAX_ARGUMENTS_AJOUT);
    return answer;
}

//------------------------------------------------- Surcharge d'opérateurs
/*
Catalogue & Catalogue::operator=(const Catalogue & unCatalogue)
// Algorithme :
//
{
} //----- Fin de operator =
 */

//-------------------------------------------- Constructeurs - destructeur

Catalogue::Catalogue(const Catalogue & unCatalogue)
// Algorithme :
//
{
#ifdef MAP
    cout << "Appel au constructeur de copie de <Catalogue>" << endl;
#endif
} //----- Fin de Catalogue (constructeur de copie)

Catalogue::Catalogue() {
    nbParcours = 0;
    nbParcoursMax = CARD_MAX_TRAJETS;

    // On déclare un pointeur temporaire
    Trajet ** CollectionTMP;

    // On instancie ce pointeur sur un tableau de pointeurs de Trajet contenant CARD_MAX_TRAJETS éléments
    CollectionTMP = new Trajet*[CARD_MAX_TRAJETS];

    //On fait pointer le vrai pointeur sur la même chose.
    Collection = CollectionTMP;

    // NOTE : à cet instant, on a un tableau de pointeurs, non instancié, qui pointent n'importe où.

#ifdef MAP
    cout << "Appel au constructeur de <Catalogue>" << endl;
#endif
} //----- Fin de Catalogue

Catalogue::~Catalogue()
// Algorithme :
//
{
#ifdef MAP
    cout << "Appel au destructeur de <Catalogue>" << endl;
#endif

#ifdef MAPV
    cout << "CATALOGUE : méthode 'Destructeur' - Lancement du delete de chaque élément du tableau " << endl;
#endif
    for (int i = 0; i < nbParcours; i++)
    {
        delete Collection[i];
#ifdef MAPV
        cout << "CATALOGUE : méthode 'Destructeur' - delete l'élément " << i << endl;
#endif
    }

#ifdef MAPV
    cout << "CATALOGUE : méthode 'Destructeur' - Delete du tableau entier" << endl;
#endif
    delete[] Collection;

} //----- Fin de ~Catalogue


//------------------------------------------------------------------ PRIVE

//----------------------------------------------------- Méthodes protégées

//------------------------------------------------------- Méthodes privées

unsigned int Catalogue::Ajuster(int delta)
// Algorithme : 
// Si delta>0 : on agrandit la structure de delta case
// Si delta=0 : on garde la structure telle quelle
// Si delta<0 : on réduit la structure de delta ou à défaut au maximum.
// On renvoit la taille finale du tableau.
{
    unsigned int answer = nbParcoursMax;

#ifdef MAP
    cout << "CATALOGUE:  : méthode 'Ajuster' appelée avec delta= " << delta << endl;
#endif

    if (delta > 0)
    {
        Trajet ** CollectionTMP = new Trajet*[nbParcoursMax + delta];
        for (int i = 0; i < nbParcours; i++)
        {
            CollectionTMP[i] = Collection[i];
        }

        delete[] Collection;

        Collection = CollectionTMP;

        nbParcoursMax = nbParcoursMax + delta;
        answer = nbParcoursMax;
#ifdef MAP
        cout << "CATALOGUE:  : méthode 'Ajuster' - Après ajout (delta >0) nbParcoursMax : " << nbParcoursMax << endl;
#endif
    }
    else if (delta < 0)
    {
        // Si il y a assez de place pour réduire
        if (-delta <= nbParcoursMax - nbParcours) // si ça passe
        {
            nbParcoursMax = nbParcoursMax + delta; // car delta < 0
            answer = nbParcoursMax;
#ifdef MAP
            cout << "CATALOGUE:  : méthode 'Ajuster' - Après réduction (delta<0) nbParcoursMax : " << nbParcoursMax << endl;
#endif
        }
        else // si il reste de la place, mais qu'on veut en supprimer trop, on reset le max à actuel
        {
            nbParcoursMax = nbParcours;
            answer = nbParcoursMax;
        }
    }
#ifdef MAP
    cout << "CATALOGUE: méthode 'Ajuster' - valeur retournée finale : " << nbParcoursMax << endl;
#endif

    return answer;
}

int Catalogue::CompteurArguments(char** arguments, int nombreArgumentsVoulus) {
    int nb_arguments_effectifs = 0;

#ifdef MAP
    cout << "CATALOGUE : méthode 'CompteurArguments' d'un Catalogue appelée " << endl;
#endif

    for (int i = 0; i < nombreArgumentsVoulus; i++)
    {
        if (arguments[i] != NULL)
        {
            nb_arguments_effectifs++;
#ifdef MAPV
            cout << "CATALOGUE : méthode 'CompteurArguments'- Arguments N°" << nb_arguments_effectifs << endl;
            cout << "CATALOGUE : méthode 'CompteurArguments'- Arguments : " << arguments[i] << endl;
#endif
        }
    }

#ifdef MAPV
    cout << "CATALOGUE : méthode 'CompteurArguments' - Valeur finale rendue : " << nb_arguments_effectifs << endl;
#endif
    return nb_arguments_effectifs;
}

bool Catalogue::estMoyenTransportValide(char* saisie)
//Pas trouvé d'idée pour le faire directement avec ChaineToMoyenTransport
{
    bool answer = false;

    for (int i = 0; i < nbMoyenTransport; i++)
    {
        if (!strcmp(saisie, moyenDeTransportChaine[i]))
        {
            answer = true;
        }
    }

    return answer;
}

moyenDeTransport Catalogue::chaineToMoyenTransport(char* saisie)
//retourne rien si n'a pas été trouvé.
{
    moyenDeTransport answer;

    for (int i = 0; i < nbMoyenTransport; i++)
    {
        if (!strcmp(saisie, moyenDeTransportChaine[i]))
        {
            //Normalement, les deux tableaux correspondent, pour faire ceci.
            answer = static_cast<moyenDeTransport> (i);
        }
    }

    return answer;
}

void Catalogue::Nettoyeur(char** aNettoyer, int tailleANettoyer) {
    for (int i = 0; i < tailleANettoyer; i++)
    {
#ifdef MAPV
        cout << "CATALOGUE : méthode 'Nettoyeur' - Nettoyage de l'élément N°" << i << endl;
#endif
        if (aNettoyer[i] != NULL)
        {
#ifdef MAPV
            cout << "CATALOGUE : méthode 'Nettoyeur' - Nettoyage de l'élément : " << aNettoyer[i] << endl;
#endif
            delete aNettoyer[i];
        }
    }

    // Il sera réalloué, si nécessaire.
    delete[] aNettoyer;
}

char** Catalogue::Parser(char* saisie, int nombreArgumentsVoulus) {
    // On a 6 arguments au maximum, on en stocke 7 pour premettre de savoir si l'utilisateur a fait n'importe quoi.
    int nb_arguments_effectifs = 0;

    //On alloue notre tableaux de pointeurs, d'arguments.
    char **answer = new char*[nombreArgumentsVoulus]; // On a maximum 6 arguments.

    //On définit notre pointeur qui servira d'index de parcours.
    char* p;

    // char *strtok(char *ptr, char *str); permet de pointer sur une chaîne de caractère étant le premier argument.
    p = strtok(saisie, " ");

    for (int i = 0; i < nombreArgumentsVoulus && p != NULL; i++)
        //tant qu'on est pas à (6+1) arguments ou qu'on a fini de trouver des espaces, on continue
    {
        answer[i] = new char[strlen(p) + 1]; // On prépare la case du premier argument avec '\0'
        strcpy(answer[i], p);
        nb_arguments_effectifs++;

        // Pour que strtok continue à parser la même chaîne de caractères, on met NULL en argument.
        p = strtok(NULL, " ");
    }

    for (int i = nb_arguments_effectifs; i < nombreArgumentsVoulus; i++)
        // On rempli les autres cases de pointeurs NULL
    {
        answer[i] = NULL;
    }

#ifdef MAPV
    cout << "CATALOGUE : méthode 'PARSER' d'un Catalogue appelée " << endl;

    for (int i = 0; i < nombreArgumentsVoulus; i++)
    {
        cout << " Arguments N°" << i << endl;
        if (answer[i] != NULL)
        {
            cout << answer[i] << endl;
        }
        else
        {
            cout << "argument null" << endl;
        }
    }
#endif

    return answer;
}
