/*************************************************************************
                           TrajetSimple  -  description
                             -------------------
    début                : 14/11/2016
    copyright            : (C) 2016 par FALCONIERI & LACHAT
    e-mail               : vincent.falconieri@insa-lyon.fr & laetita.lachat@insa-lyon.fr
 *************************************************************************/

//---------- Réalisation de la classe <TrajetSimple> (fichier TrajetSimple.cpp) ------------

//---------------------------------------------------------------- INCLUDE

//-------------------------------------------------------- Include système
using namespace std;
#include <iostream>
#include <cstring>

//------------------------------------------------------ Include personnel
#include "TrajetSimple.h"

//------------------------------------------------------------- Constantes

//---------------------------------------------------- Variables de classe

//----------------------------------------------------------- Types privés


//----------------------------------------------------------------- PUBLIC
//-------------------------------------------------------- Fonctions amies

//----------------------------------------------------- Méthodes publiques
// type TrajetSimple::Méthode ( liste de paramètres )
// Algorithme :
//
//{
//} //----- Fin de Méthode

const char * TrajetSimple::getVilleA() {
#ifdef MAP
    cout << "TRAJET SIMPLE : méthode 'getVilleA' d'un TRAJET SIMPLE appelée " << endl;
    cout << villeDepart << endl;
#endif
    return villeDepart;
}

const char * TrajetSimple::getVilleB() {
#ifdef MAP
    cout << "TRAJET SIMPLE : méthode 'getVilleB' d'un TRAJET SIMPLE appelée " << endl;
    cout << villeDestination << endl;
#endif
    return villeDestination;
}

const moyenDeTransport TrajetSimple::getMoyen() {
#ifdef MAP
    cout << "TRAJET SIMPLE : méthode 'getMoyen' d'un TRAJET SIMPLE appelée " << endl;
#endif
    AfficherEnumMoyen(moyenTransport);

    return moyenTransport;
}

void TrajetSimple::Afficher() {
#ifdef MAP
    cout << "TRAJET SIMPLE : méthode 'Affichage' d'un TRAJET SIMPLE appelée " << endl;
#endif
    cout << "de " << villeDepart << " à " << villeDestination << " en ";

    AfficherEnumMoyen(moyenTransport);
}

Trajet* TrajetSimple::getCopy() const
{
    Trajet* answer = new TrajetSimple(*this);
    
    return answer;
}

//------------------------------------------------- Surcharge d'opérateurs

/*
TrajetSimple & TrajetSimple::operator=(const TrajetSimple & unTrajetSimple)
{
    // QUESTION : On ne renvoit pas un booléan ? Oo
    
    bool answer = false;
    //Pour question de lisibilité
    bool moy = strcmp(unTrajetSimple.moyenTransport, this->moyenTransport) ;
    bool vilA = strcmp(unTrajetSimple.villeDepart, this->villeDepart);
    bool vilB = strcmp(unTrajetSimple.villeDestination, this->villeDestination);
    
    if( moy == 0 && vilA ==0 && vilB == 0 )
    {
        answer = true;
    }
    
    return answer;
    
} //----- Fin de operator =
 */

//-------------------------------------------- Constructeurs - destructeur

TrajetSimple::TrajetSimple(const TrajetSimple & unTrajetSimple) {
    // On initialise et on copie les valeurs
    this->villeDepart = new char[strlen(unTrajetSimple.villeDepart) + 1]; // Caractère '\0'
    strcpy(this->villeDepart, unTrajetSimple.villeDepart);

    this->villeDestination = new char[strlen(unTrajetSimple.villeDestination) + 1]; // Caractère '\0'
    strcpy(this->villeDestination, unTrajetSimple.villeDestination);

    this->moyenTransport = unTrajetSimple.moyenTransport;

#ifdef MAP
    cout << "Appel au constructeur de copie de <TrajetSimple>" << endl;
#endif
} //----- Fin de TrajetSimple (constructeur de copie)

TrajetSimple::TrajetSimple(const char * villeDepart, const char * villeArrivee, const moyenDeTransport transport) : Trajet() {
    // On initialise les attributs d'un Trajet Simple
    this->villeDepart = new char[strlen(villeDepart) + 1]; // Caractère '\0'
    strcpy(this->villeDepart, villeDepart);

    this->villeDestination = new char[strlen(villeArrivee) + 1]; // Caractère '\0'
    strcpy(this->villeDestination, villeArrivee);

    this->moyenTransport = transport;

#ifdef MAP
    cout << "Appel au constructeur de <TrajetSimple>" << endl;
#endif
} //----- Fin de TrajetSimple

TrajetSimple::~TrajetSimple() {
    // On dés-salloue les pointeurs de l'instance
    delete[] this->villeDepart;
    delete[] this->villeDestination;

#ifdef MAP
    cout << "Appel au destructeur de <TrajetSimple>" << endl;
#endif
} //----- Fin de ~TrajetSimple


//------------------------------------------------------------------ PRIVE

//----------------------------------------------------- Méthodes protégées

//------------------------------------------------------- Méthodes privées

void TrajetSimple::AfficherEnumMoyen(moyenDeTransport ceMoyen) {
    cout << moyenDeTransportChaine[ceMoyen];
}


