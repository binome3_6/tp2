/*************************************************************************
                           TRAJET  -  description
                             -------------------
    début                : 14/11/2016
    copyright            : (C) 2016 par FALCONIERI & LACHAT
    e-mail               : vincent.falconieri@insa-lyon.fr & laetita.lachat@insa-lyon.fr
 *************************************************************************/

//---------- Réalisation de la classe <TRAJET> (fichier TRAJET.cpp) ------------

//---------------------------------------------------------------- INCLUDE

//-------------------------------------------------------- Include système

using namespace std;
#include <iostream>
#include <cstring>

//------------------------------------------------------ Include personnel
#include "Trajet.h"

//------------------------------------------------------------- Constantes

//---------------------------------------------------- Variables de classe

//----------------------------------------------------------- Types privés


//----------------------------------------------------------------- PUBLIC
//-------------------------------------------------------- Fonctions amies

//----------------------------------------------------- Méthodes publiques
// type Trajet::Méthode ( liste de paramètres )
// Algorithme :
//
//{
//} //----- Fin de Méthode

const char * Trajet::getVilleA() {
    char * answer;
#ifdef MAP
    cout << "ERREUR : méthode 'getVilleA' d'un TRAJET appelée " << endl;
#endif
    return answer;
}
// Mode d'emploi :
// Permet de récupérer la ville de départ d'un trajet donné.
// Contrat :
// aucun

const char * Trajet::getVilleB() {
    char * answer;
#ifdef MAP
    cout << "ERREUR : méthode 'getVilleB' d'un TRAJET appelée " << endl;
#endif
    return answer;
}
// Mode d'emploi :
// Permet de récupérer la ville d'arrivée d'un trajet donné.
// Contrat :
// aucun

void Trajet::Afficher() {
#ifdef MAP
    cout << "ERREUR : méthode 'Afficher' d'un TRAJET appelée " << endl;
#endif
}
// Mode d'emploi :
// Permet de fournir un affichage du trajet sur la sortie standard.
// du format : "de A à B en MT1"
// Contrat :
// aucun

Trajet* Trajet::getCopy() const {
    Trajet* answer;
#ifdef MAP
    cout << "ERREUR : méthode 'GetCopy' d'un Trajet (vide) appelée " << endl;
#endif
    return answer;
}

//------------------------------------------------- Surcharge d'opérateurs

/*
Trajet & Trajet::operator=(const Trajet & unTrajet)
// Algorithme :
//
{
} //----- Fin de operator =
 */

//-------------------------------------------- Constructeurs - destructeur

Trajet::Trajet(const Trajet & unTrajet)
// Algorithme :
//
{
#ifdef MAP
    cout << "Appel au constructeur de copie de <Trajet>" << endl;
#endif
} //----- Fin de Trajet (constructeur de copie)

Trajet::Trajet()
// Algorithme :
//
{
#ifdef MAP
    cout << "Appel au constructeur de <Trajet>" << endl;
#endif
} //----- Fin de Trajet

Trajet::~Trajet()
// Algorithme :
//
{
#ifdef MAP
    cout << "Appel au destructeur de <Trajet>" << endl;
#endif
} //----- Fin de ~Trajet


//------------------------------------------------------------------ PRIVE

//----------------------------------------------------- Méthodes protégées

//------------------------------------------------------- Méthodes privées

