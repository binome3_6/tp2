/*************************************************************************
                           TrajetCompose  -  description
                             -------------------
    début                : 14/11/2016
    copyright            : (C) 2016 par FALCONIERI & LACHAT
    e-mail               : vincent.falconieri@insa-lyon.fr & laetita.lachat@insa-lyon.fr
 *************************************************************************/
//---------- Réalisation de la classe <TrajetCompose> (fichier TrajetCompose.cpp) ------------

//---------------------------------------------------------------- INCLUDE
//-------------------------------------------------------- Include système
using namespace std;
#include <iostream>
#include <cstring>
//------------------------------------------------------ Include personnel
#include "TrajetCompose.h"
#include "TrajetSimple.h"
//------------------------------------------------------------- Constantes
//----------------------------------------------------------------- PUBLIC
//-------------------------------------------------------- Fonctions amies
//----------------------------------------------------- Méthodes publiques

const char * TrajetCompose::GetVilleA()
// Algorithme : On récupère la ville du trajet de la première case du tableau.
// Note : récursivement, si la première case comporte un trajet composé, cela fonctionne.
// Contrat : aucun
{
    const char* answer = VILLE_VIDE;
#ifdef MAP
    cout << "TRAJET COMPOSE : méthode 'getVilleA' d'un TRAJET Composé appelée " << endl;
#endif
    if (nbTrajets == 0)
    {
        answer = VILLE_VIDE;
#ifdef MAPV
        cout << "TRAJET COMPOSE - méthode 'getVilleA' - answer : " << answer << endl;
#endif
    }
    else
    {
        // Answer, un pointeur, prendra la valeur de l'adresse qui pointe sur l'objet "VilleA"
        // si doute sur l'écriture : voir la copie de l'explication dans le fichier projet.
        answer = &*listeTrajets[0]->GetVilleA();
    }

    return answer;
}//----- Fin de getVilleA

const char * TrajetCompose::GetVilleB()
// Algorithme : On récupère la ville du trajet de la dernière case remplie du tableau.
// Note : récusrivement, si la dernière case comporte un trajet composé, cela fonctionne.
// Contrat : aucun
{
    const char * answer = VILLE_VIDE;

#ifdef MAP
    cout << "TRAJET COMPOSE : méthode 'getVilleB' d'un TRAJET Composé appelée " << endl;
#endif

    if (nbTrajets == 0)
    {
        answer = VILLE_VIDE;
#ifdef MAPV
        cout << "TRAJET COMPOSE - méthode 'getVilleB' - answer : " << answer << endl;
#endif
    }
    else
    {
        //On doit décaler avec le "-1" pour revenir à l'indice 0 si il y a un seul objet
        // Answer, un pointeur, prendra la valeur de l'adresse qui pointe sur l'objet "VilleB"
        // si doute sur l'écriture : voir la copie de l'explication dans le fichier projet.
        answer = &*listeTrajets[nbTrajets - 1]->GetVilleB();
#ifdef MAP
        cout << "TRAJET COMPOSE - méthode 'getVilleB' - Accès au N° : " << nbTrajets - 1 << endl;
#endif
    }

    return answer;
}//----- Fin de getVilleB

void TrajetCompose::Afficher()
// Algorithme : On appelle l'affichage de chaque case du tableau remplie, en appellant
// la fonction afficher de chaque Trajet.
// Note : récursivement, si une case du tableau comporte un trajet composé, cela fonctionne encore !
// Contrat : aucun
{
#ifdef MAP
    cout << "TRAJET COMPOSE : méthode 'Affichage' d'un TRAJET Composé appelée " << endl;
#endif
    for (int i = 0; i < nbTrajets; i++)
    {
#ifdef MAP
        cout << "TRAJET COMPOSE : méthode 'Affichage'- Affichage du N°" << i << endl;
#endif
        listeTrajets[i]->Afficher();
        if (i != nbTrajets - 1)
        {
            cout << " - ";
        }
    }
    cout << endl;
}//----- Fin de Afficher

bool TrajetCompose::AjouterTrajetSimple(char * villeDepart, char * villeArrivee, MoyenDeTransport transport)
// Permet d'ajouter un trajet simple, en donnant comme paramètre directement les paramètres 
// nécessaires au constructeur de trajet simple.
// Se réferer donc pour de plus ample détails au constructeur de trajet simple.
// Le trajet simple est ajouté, comme composante, du trajet composé.
// Contrat : aucun
// Algorithme : aucun
{
#ifdef MAP
    cout << "TRAJET COMPOSE : méthode 'AjouterTrajetSimple' appelée " << endl;
#endif
    bool answer = false;

    //Si il y a assez de place
    if (nbTrajets < nbMaxTrajets)
    {
#ifdef MAPV
        cout << "TRAJET COMPOSE : méthode 'AjouterTrajetSimple' - il reste de la place " << endl;
#endif            
        //Notons que cette vérificaiton n'est plus nécessaire, mainteant que l'ajout de trajet simple 
        // se fait directement par une saisie "sécurisée".
        //Mais même ainsi, il est préférable que la structure de données vérifie sont intégrité.
        if (nbTrajets == 0 || !strcmp(listeTrajets[nbTrajets - 1]->GetVilleB(), villeDepart))
        {
            //Si la ville B du précédent est égal à la ville A de l'ajouté OU qu'on ajoute la première ville

            listeTrajets[nbTrajets] = new TrajetSimple(villeDepart, villeArrivee, transport);
            // On instancie au bout du pointeur de la case (n) un objet "TrajetSimple"

#ifdef MAP
            cout << "TRAJET COMPOSE : méthode 'AjouterTrajetSimple' - un trajet simple a été ajouté " << endl;
#endif
            nbTrajets++;
            answer = true;
        }
        else
        {
            cout << "Erreur : La ville d'arrivée du précédent trajet n'est pas la ville de départ du trajet tentant d'être ajouté." << endl;
#ifdef MAP
            cout << "TRAJET COMPOSE - méthode 'AjouterTrajetSimple' - ville précédente : " 
                    << listeTrajets[nbTrajets - 1]->GetVilleB() << endl;
#endif
        }
    }
    else
    {
#ifdef MAPV
        cout << "TRAJET COMPOSE : méthode 'AjouterTrajetSimple' - il n'y a plus de place, on doit ajuster " << endl;
#endif
        //Sinon, il faut réajuster. Ici on double la taille à chaque fois.
        this->ajuster(nbMaxTrajets);
        //De nouveau on tente l'ajoût
        this->AjouterTrajetSimple(villeDepart, villeArrivee, transport);
    }

    return answer;
}//----- Fin de AjouterTrajetSimple

//------------------------------------------------- Surcharge d'opérateurs
//-------------------------------------------- Constructeurs - destructeur

TrajetCompose::TrajetCompose() : Trajet()
// Constructeur de TrajetCompose, qui instancie les attributs de la classe convenablement.
// Contrat : aucun
// Algorithme : aucun
{
    nbTrajets = 0;
    nbMaxTrajets = CARD_MAX;

    // On déclare un pointeur temporaire
    Trajet ** listeTrajetsTMP;
    // On instancie ce pointeur sur un tableau de pointeurs de Trajet contenant CARD_MAX éléments
    listeTrajetsTMP = new Trajet*[CARD_MAX];

    //On fait pointer le vrai pointeur sur la même chose.
    listeTrajets = listeTrajetsTMP;

    // NOTE : à cet instant, on a un tableau de pointeurs, non instancié, qui pointent n'importe où.

#ifdef MAP
    cout << "Appel au constructeur de <TrajetCompose>" << endl;
#endif
} //----- Fin de TrajetCompose

TrajetCompose::~TrajetCompose()
// Destructeur de TrajetCompose, détruit l'objet convenablement.
// Contrat : aucun
// Algorithme : aucun
{
#ifdef MAP
    cout << "Appel au destructeur de <TrajetCompose>" << endl;
#endif

#ifdef MAPV
    cout << "TRAJET COMPOSE : méthode 'Destructeur' - Lancement du delete de chaque élément du tableau " << endl;
#endif
    for (int i = 0; i < nbTrajets; i++)
    {
        delete listeTrajets[i];
#ifdef MAPV
        cout << "TRAJET COMPOSE : méthode 'Destructeur' - delete l'élément " << i << endl;
#endif
    }

#ifdef MAPV
    cout << "TRAJET COMPOSE : méthode 'Destructeur' - Delete du tableau entier" << endl;
#endif
    delete[] listeTrajets;

} //----- Fin de ~TrajetCompose

//------------------------------------------------------------------ PRIVE
//----------------------------------------------------- Méthodes protégées
//------------------------------------------------------- Méthodes privées

unsigned int TrajetCompose::ajuster(int delta)
//Permet d'ajuster en positif comme en négatif la taille de la liste de Trajets d'un Trajet composé.
// Notons que nous nous en servons principalement pour agrandir la structure de données.
// Algorithme : 
// Si delta>0 : on agrandit la structure de delta case
// Si delta=0 : on garde la structure telle quelle
// Si delta<0 : on réduit la structure de delta ou à défaut au maximum.
// On renvoit la taille finale du tableau.
// Contrat : aucun
{
    unsigned int answer = nbMaxTrajets;
#ifdef MAP
    cout << "TRAJET COMPOSE : méthode 'Ajuster' appelée avec delta= " << delta << endl;
#endif

    if (delta > 0)
    {
        Trajet ** listeTrajetsTMP = new Trajet*[nbMaxTrajets + delta];
        for (int i = 0; i < nbTrajets; i++)
        {
            listeTrajetsTMP[i] = listeTrajets[i];
        }

        delete[] listeTrajets;

        listeTrajets = listeTrajetsTMP;

        nbMaxTrajets = nbMaxTrajets + delta;
        answer = nbMaxTrajets;
#ifdef MAP
        cout << "TRAJET COMPOSE : méthode 'Ajuster' - Après ajout (delta >0) nbMaxTrajets : " << nbMaxTrajets << endl;
#endif
    }
    else if (delta < 0)
    {
        // Si il y a assez de place pour réduire
        if (-delta <= nbMaxTrajets - nbTrajets) // si ça passe
        {
            nbMaxTrajets = nbMaxTrajets + delta; // car delta < 0
            answer = nbMaxTrajets;
#ifdef MAP
            cout << "TRAJET COMPOSE : méthode 'Ajuster' - Après réduction (delta<0) nbMaxTrajets : " << nbMaxTrajets << endl;
#endif
        }
        else // si il reste de la place, mais qu'on veut en supprimer trop, on reset le max à actuel
        {
            nbMaxTrajets = nbTrajets;
            answer = nbMaxTrajets;
        }
    }
#ifdef MAP
    cout << "TRAJET COMPOSE : méthode 'Ajuster' - valeur retournée finale : " << nbMaxTrajets << endl;
#endif

    return answer;
} //----- Fin de Ajuster