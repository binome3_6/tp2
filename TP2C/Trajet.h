/*************************************************************************
                           TRAJET  -  description
                             -------------------
    début                : 14/11/2016
    copyright            : (C) 2016 par FALCONIERI & LACHAT
    e-mail               : vincent.falconieri@insa-lyon.fr & laetita.lachat@insa-lyon.fr
 *************************************************************************/
//---------- Interface de la classe <TRAJET> (fichier Trajet.h) ----------------
#if ! defined ( TRAJET_H )
#define TRAJET_H
//--------------------------------------------------- Interfaces utilisées
//------------------------------------------------------------- Constantes
static const char* const VILLE_VIDE = (char*) "vide"; // L'ISO C++ s'attend à voir un string. En castant, on résoud.
//------------------------------------------------------------------ Types

//------------------------------------------------------------------------
// Rôle de la classe <Trajet>
// La classe trajet représente le parent des classes "Trajet Simple" et "Trajet composé"
// Elle permet d'utiliser le polymorphisme pour les méthodes de classes communes aux deux
// entités citées précedemment. 
//------------------------------------------------------------------------
class Trajet 
{
    //----------------------------------------------------------------- PUBLIC
public:
    //----------------------------------------------------- Méthodes publiques
    virtual const char * GetVilleA();
    // Mode d'emploi :
    // Permet de récupérer la ville de départ d'un trajet donné.
    // Définition vide.
    // Contrat : aucun

    virtual const char * GetVilleB();
    // Mode d'emploi :
    // Permet de récupérer la ville d'arrivée d'un trajet donné.
    // Définition vide.
    // Contrat : aucun

    virtual void Afficher();
    // Mode d'emploi :
    // Permet de fournir un affichage du trajet sur la sortie standard.
    // Définition vide.
    // Contrat : aucun

    //------------------------------------------------- Surcharge d'opérateurs
    Trajet & operator=(const Trajet & unTrajet);
    // Mode d'emploi :
    // Déclaré et non défini pour éviter l'utilisation de l'opérateur par défaut

    //-------------------------------------------- Constructeurs - destructeur
    Trajet(const Trajet & unTrajet);
    // Mode d'emploi :
    // Déclaré mais non défini, pour éviter que le compilateur en génère un par défaut.

    Trajet();
    // Mode d'emploi :
    // Permet d'instancier un trajet.
    // Est pisté.
    
    virtual ~Trajet();
    // Mode d'emploi :
    // Permet de détruire un trajet.
    // Est pisté.

    //------------------------------------------------------------------ PRIVE
protected:
    //----------------------------------------------------- Méthodes protégées
private:
    //------------------------------------------------------- Méthodes privées
protected:
    //----------------------------------------------------- Attributs protégés
private:
    //------------------------------------------------------- Attributs privés
};
#endif // TRAJET_H