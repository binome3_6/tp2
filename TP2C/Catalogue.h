/*************************************************************************
                           CATALOGUE  -  description
                             -------------------
        début                : 14/11/2016
    copyright            : (C) 2016 par FALCONIERI & LACHAT
    e-mail               : vincent.falconieri@insa-lyon.fr & laetita.lachat@insa-lyon.fr
 *************************************************************************/
//---------- Interface de la classe <Catalogue> (fichier Catalogue.h) ----------------
#if ! defined ( CATALOGUE_H )
#define CATALOGUE_H
//--------------------------------------------------- Interfaces utilisées
#include "Trajet.h"
#include "TrajetCompose.h"
#include "TrajetSimple.h"
//------------------------------------------------------------- Constantes
static const int CARD_MAX_TRAJETS = 10;
static const int LONGUEUR_NOM_VILLE = 50;
//------------------------------------------------------------------ Types

//------------------------------------------------------------------------
// Rôle de la classe <Catalogue>
// Catalogue est une collection de parcours 
// est ordonnee d'objets heterogenes (trajets simples et trajets simples)
//------------------------------------------------------------------------

class Catalogue 
{
    //----------------------------------------------------------------- PUBLIC
public:
    //----------------------------------------------------- Méthodes publiques
    void Afficher();
    // Mode d'emploi : 
    // Affiche tous les trajets possibles contenus dans catalogue de la forme
    // Parcours N°x : de VilleA à VilleB en MoyenTransport - de VilleB à VilleC ...
    // Contrat : aucun
    
    void Rechercher();
    // Mode d'emploi : 
    // Recherche d'un parcours avec les entrées faites par l'utilisateur (via l'IHM)
    // Recherche automatiquement avec la méthode la plus exhaustive possible.
    // Contrat : aucun
    
    void Menu();
    // Mode d'emploi : Affiche le menu pour gérer le catalogue, donc
    // Affiche une interface IHM unique pour appeler les autres fonctions.
    // Contrat : aucun
    
    bool Ajouter(); 
    // Mode d'emploi : 
    // Ajoute un trajet au catalogue avec les entrées faites par l'utilisateur (via l'IHM)
    // Si l'ajout a été un succès, la fonction renvoie "true"
    // Contrat : aucun
    
    void RechercherSimple(const char * villeA, const char * villeB);
    // Mode d'emploi : Recherche d'un parcours avec un algorithme simple (non exhaustif)
    // Les deux pointeurs de caractères correspondent à : 
    // le premier donne la ville de départ
    // le second la ville d'arrivée du parcours recherché
    // Contrat : aucun

    void RechercherComplexe(const char * villeA, const char * villeB, bool estBouclante);
    // Mode d'emploi : Recherche d'un parcours avec un algorithme long (exhaustif)
    // Les deux pointeurs de caractères correspondent à : 
    // le premier donne la ville de départ
    // le second la ville d'arrivée du parcours recherché
    // Le booléan correspond à l'autorisation de repasser par le point de départ (true) ou non (false).
    // Contrat : aucun

    //------------------------------------------------- Surcharge d'opérateurs
    Catalogue & operator=(const Catalogue & unCatalogue);
    // Mode d'emploi :
    // Déclaré et non défini pour éviter l'utilisation de l'opérateur par défaut

    //-------------------------------------------- Constructeurs - destructeur
    Catalogue(const Catalogue & unCatalogue);
    // Mode d'emploi (constructeur de copie) :
    // Permet de fournir une copie du catalogue donné en paramètre.
    // Déclaré mais non défini, pour éviter que le compilateur en génère un par défaut.

    Catalogue();
    // Mode d'emploi : 
    // Constructeur de catalogue. Créé un catalogue vide.

    virtual ~Catalogue();
    // Mode d'emploi :
    // Permet de détruire un catalogue

    //------------------------------------------------------------------ PRIVE
private:
    //------------------------------------------------------- Méthodes privées
    unsigned int ajuster(int delta);
    static MoyenDeTransport saisieTransport();
    static int saisieChoixMenu();
    bool rechercherInt(int* ListeInt, int tailleListe, int valeurATrouver);
    int rechercherRecursive(const char * villeOriginel, int indiceDebut, 
        const char * villeB, int depth, Trajet ** ListeParcoursTMP, 
            int* ListeParcoursIndices, int nbParcoursTrouves, bool estBouclante);
    char* normalise (char* motNonNormalise); 

protected:
    //----------------------------------------------------- Attributs protégés
    int nbParcours;
    int nbParcoursMax;
    Trajet ** collectionParcours;

    //------------------------------------------------------- Attributs privés
};
#endif // CATALOGUE_H