/*************************************************************************
                           TrajetSimple  -  description
                             -------------------
    début                : 14/11/2016
    copyright            : (C) 2016 par FALCONIERI & LACHAT
    e-mail               : vincent.falconieri@insa-lyon.fr & laetita.lachat@insa-lyon.fr
 *************************************************************************/
//---------- Interface de la classe <TrajetSimple> (fichier TrajetSimple.h) ----------------
#if ! defined ( TrajetSimple_H )
#define TrajetSimple_H
//--------------------------------------------------- Interfaces utilisées
#include "Trajet.h"
//------------------------------------------------------------- Constantes
static const int NB_MOYEN_TRANSPORT = 6;
static const char* MOYEN_DE_TRANSPORT_CHAINE[] = {"voiture", "train", "bateau", "avion", "marche", "vélo"};
//------------------------------------------------------------------ Types
typedef enum 
{
    VOITURE, TRAIN, BATEAU, AVION, MARCHE, VELO
} MoyenDeTransport;

//------------------------------------------------------------------------
// Rôle de la classe <TrajetSimple>
// Un trajet simple gère un trajet "unaire", un déplacement élémentaire entre 
// deux villes, effectué grâce à un moyen de transport spécifié. 
//------------------------------------------------------------------------

class TrajetSimple : public Trajet 
{
    //----------------------------------------------------------------- PUBLIC
public:
    //----------------------------------------------------- Méthodes publiques
    virtual const char * GetVilleA();
    // Mode d'emploi :
    // Affiche la ville de depart
    // Contrat : aucun

    virtual const char * GetVilleB();
    // Mode d'emploi : 
    // Affiche la ville de destination
    // Contrat : aucun

    virtual const MoyenDeTransport GetMoyen();
    // Mode d'emploi : 
    // Affiche le moyen de transport
    // Contrat : aucun

    virtual void Afficher();
    // Mode d'emploi : 
    // Affiche le trajet avec la syntaxe : “de VilleA à VilleB en Moyen”
    // Contrat : aucun

    //------------------------------------------------- Surcharge d'opérateurs
    TrajetSimple & operator=(const TrajetSimple & unTrajetSimple);
    // Déclaré et non défini pour éviter l'utilisation de l'opérateur par défaut
    
    //-------------------------------------------- Constructeurs - destructeur
    TrajetSimple(const TrajetSimple & unTrajetSimple);
    // Mode d'emploi (constructeur de copie) : 
    // Permet de fournir une copie du trajet simple donné en paramètre.
    // Déclaré mais non défini, pour éviter que le compilateur en génère un par défaut.

    TrajetSimple(const char * villeDepart, const char * villeArrivee, const MoyenDeTransport transport);
    // Mode d'emploi : 
    // Créé un TrajetSimple avec les arguments passés en paramètre :
    // villeDepart : la ville de départ du trajet
    // villeArrivee : la ville d'arrivée du trajet
    // moyenDeTransport : le moyen de transport du trajet
    // Contrat : les trois arguments doivent être valides
 
    virtual ~TrajetSimple();
    // Mode d'emploi :
    // Permet de détruire un trajet simple

    //------------------------------------------------------------------ PRIVE
protected:
    //----------------------------------------------------- Méthodes protégées
private:
    //------------------------------------------------------- Méthodes privées
    static void afficherEnumMoyen(MoyenDeTransport ceMoyen);

protected:
    //----------------------------------------------------- Attributs protégés
    char * villeDepart;
    char * villeDestination;
    MoyenDeTransport moyenTransport;

private:
    //------------------------------------------------------- Attributs privés
};
#endif // TrajetSimple_H

