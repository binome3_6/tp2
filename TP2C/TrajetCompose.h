/*************************************************************************
                           TRAJET  -  description
                             -------------------
    début                : 14/11/2016
    copyright            : (C) 2016 par FALCONIERI & LACHAT
    e-mail               : vincent.falconieri@insa-lyon.fr & laetita.lachat@insa-lyon.fr
 *************************************************************************/
//---------- Interface de la classe <TrajetCompose> (fichier TrajetCompose.h) ----------------
#if ! defined ( TRAJETCOMPOSE_H )
#define TRAJETCOMPOSE_H
//--------------------------------------------------- Interfaces utilisées
#include "Trajet.h"
#include "TrajetSimple.h"
//------------------------------------------------------------- Constantes
static const int CARD_MAX = 10;
//------------------------------------------------------------------ Types

//------------------------------------------------------------------------
// Rôle de la classe <TrajetCompose>
// Un trajet composé est un "métatrajet", qui comporte plusieurs autres trajets plus élémentaires.
// Les trajets à l'intérieur d'un trajetComposé sont de type "Trajet"
//------------------------------------------------------------------------
class TrajetCompose : public Trajet 
{
    //----------------------------------------------------------------- PUBLIC
public:
    //----------------------------------------------------- Méthodes publiques
    virtual const char * GetVilleA();
    // Mode d'emploi :
    // Permet de récupérer la ville de départ du trajet composé global.
    // Contrat : aucun

    virtual const char * GetVilleB();
    // Mode d'emploi :
    // Permet de récupérer la ville d'arrivée du trajet composé global.
    // Contrat : aucun

    virtual void Afficher();
    // Mode d'emploi :
    // Permet de fournir un affichage de l'ensemble des trajets compris dans le 
    // trajet composé sur la sortie standard.
    // du format : "de A à B en MT1 - de Y à Z en MT2"
    // Contrat : aucun

    bool AjouterTrajetSimple(char * villeDepart, char * villeArrivee, MoyenDeTransport transport);
    // Mode d'emploi : On lui donne en paramètre les composants d'un trajet simple
    // qui sera ajouté à la liste des trajets composant le trajet composé courant.
    // Contrat : aucun

    //------------------------------------------------- Surcharge d'opérateurs
    TrajetCompose & operator=(const TrajetCompose & unTrajetCompose);
    // Déclaré et non initialisé pour éviter l'utilisation de l'opérateur par défaut

    //-------------------------------------------- Constructeurs - destructeur
    TrajetCompose(const TrajetCompose & unTrajetCompose);
    // Mode d'emploi (constructeur de copie) :
    // Déclaré et non initialisé pour éviter l'utilisation de l'opérateur par défaut

    TrajetCompose();
    // Mode d'emploi :
    // Constructeur de TrajetCompose. Créé un TrajetCompose vide.

    virtual ~TrajetCompose();
    // Mode d'emploi :
    // Permet de détruire un TrajetCompose

    //------------------------------------------------------------------ PRIVE
protected:
    //----------------------------------------------------- Méthodes protégées
private:
    //------------------------------------------------------- Méthodes privées
    unsigned int ajuster(int delta);
    
protected:
    //----------------------------------------------------- Attributs protégés
    int nbTrajets;
    int nbMaxTrajets;
    Trajet ** listeTrajets;

private:
    //------------------------------------------------------- Attributs privés
};
#endif // TRAJETCOMPOSE_H

