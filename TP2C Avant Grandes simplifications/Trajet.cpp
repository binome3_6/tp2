/*************************************************************************
                           TRAJET  -  description
                             -------------------
    début                : 14/11/2016
    copyright            : (C) 2016 par FALCONIERI & LACHAT
    e-mail               : vincent.falconieri@insa-lyon.fr & laetita.lachat@insa-lyon.fr
 *************************************************************************/
//---------- Réalisation de la classe <TRAJET> (fichier TRAJET.cpp) ------------

//---------------------------------------------------------------- INCLUDE
//-------------------------------------------------------- Include système
using namespace std;
#include <iostream>
#include <cstring>
//------------------------------------------------------ Include personnel
#include "Trajet.h"
#include "TrajetCompose.h" // nécessaire pour accéder à "villeVide" - 
//------------------------------------------------------------- Constantes

//----------------------------------------------------------------- PUBLIC

//----------------------------------------------------- Méthodes publiques
// type Trajet::Méthode ( liste de paramètres )
// Algorithme :
//
//{
//} //----- Fin de Méthode

const char * Trajet::getVilleA()
{
#ifdef MAP
    cout << "ERREUR : méthode 'getVilleA' d'un TRAJET appelée " << endl;
#endif
    return villeVide;
}

const char * Trajet::getVilleB()
{
#ifdef MAP
    cout << "ERREUR : méthode 'getVilleB' d'un TRAJET appelée " << endl;
#endif
    return villeVide;
}

void Trajet::Afficher()
{
#ifdef MAP
    cout << "ERREUR : méthode 'Afficher' d'un TRAJET appelée " << endl;
#endif
}

//------------------------------------------------- Surcharge d'opérateurs

//-------------------------------------------- Constructeurs - destructeur

Trajet::Trajet(const Trajet & unTrajet)
// Algorithme :
//
{
#ifdef MAP
    cout << "Appel au constructeur de copie de <Trajet>" << endl;
#endif
} //----- Fin de Trajet (constructeur de copie)

Trajet::Trajet()
// Algorithme :
//
{
#ifdef MAP
    cout << "Appel au constructeur de <Trajet>" << endl;
#endif
} //----- Fin de Trajet

Trajet::~Trajet()
// Algorithme :
//
{
#ifdef MAP
    cout << "Appel au destructeur de <Trajet>" << endl;
#endif
} //----- Fin de ~Trajet

//------------------------------------------------------------------ PRIVE

//----------------------------------------------------- Méthodes protégées

//------------------------------------------------------- Méthodes privées

