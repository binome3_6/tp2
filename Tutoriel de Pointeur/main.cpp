/*************************************************************************
                           MAIN  -  description
                             -------------------
        début                : 14/11/2016
    copyright            : (C) 2016 par FALCONIERI & LACHAT
    e-mail               : vincent.falconieri@insa-lyon.fr & laetita.lachat@insa-lyon.fr
 *************************************************************************/

//---------- Réalisation du module <MAIN> (fichier MAIN.cpp) ---------------

/////////////////////////////////////////////////////////////////  INCLUDE
//-------------------------------------------------------- Include système
#include <iostream>
#include <cstring>
using namespace std;
//------------------------------------------------------ Include personnel

//////////////////////////////////////////////////////////////////  PRIVE
//------------------------------------------------------------- Constantes

//------------------------------------------------------------------ Types

//---------------------------------------------------- Variables statiques

//------------------------------------------------------ Fonctions privées
//static type nom ( liste de paramètres )
// Mode d'emploi :
//
// Contrat :
//
// Algorithme :
//
//{
//} //----- fin de nom

static void test1() {

}

//////////////////////////////////////////////////////////////////  PUBLIC
//---------------------------------------------------- Fonctions publiques

int main()
// Algorithme :
//
{
    cout << "Debut fonction MAIN " << endl;
    //test1(); 

    {
        // On déclare un pointeur sur un type T = int
        int * pt;
        // On instancie (alloue la mémoire) de ce pointeur à un tableau de 5 objets de type T = int
        pt = new int[5];
        // On range dans la case 3 de ce tableau la valeur du type T voulu (un int = 99 ici)
        pt[3] = 99;
        // On aurait aussi pu faire : 
        *(pt + 3) = 99;

        cout << pt[3] << endl;
        cout << *(pt + 3) << endl;
    }

    {
        // On déclare un pointeur de type T = int *
        int * * pt;
        // On instancie (alloue la mémoire) de ce pointeur à un tableau de 5 objets de type T = int*
        pt = new int *[5];
        // L'objet dans une case est un pointeur.
        // on instancie (alloue la mémoire) de l'objet contenu dans la case 3. L'objet pointé est de type "int"
        pt[3] = new int;
        // On range dans la case pointé par la case 3 du tableau la valeur du type int voulu (un int = 99 ici)
        *pt[3] = 99;
        // On aurait aussi pu faire : 
        *(*(pt + 3)) = 99;

        cout << *pt[3] << endl;
        cout << *(*(pt + 3)) << endl;
    }

	char* tampon = new char[80];
	cin.getline(tampon, 80);
	cout << tampon << endl;
	
	int a=0, b=1,c=2;
	if(a){
		cout << "a=0" << endl;
	}
	if(b){
		cout << "b=1" << endl;
	}
	if(c){
		cout << "c=2" << endl;
	}

    return 0;

} //----- fin de main

