/*************************************************************************
                           MAIN  -  description
                             -------------------
        début                : 14/11/2016
    copyright            : (C) 2016 par FALCONIERI & LACHAT
    e-mail               : vincent.falconieri@insa-lyon.fr & laetita.lachat@insa-lyon.fr
 *************************************************************************/
#include <iostream>
using namespace std;
#include "Catalogue.h"
#include "Trajet.h"
#include <cstring>

static void testTableauPointeur() {
    char * test = new char[5];
    strcpy(test, "1234");
    cout << test << endl;
    delete test;
    /*
    int ** listeTrajets;
    
     *listeTrajets = new int[5];
     **listeTrajets[1] = new int[1];
    cout << listeTrajets[1] << endl;

    // un tableau de pointeurs vers des char 
    char *tableau[3] = { 'a' , 'b' , 'c' };
 
    // p est un pointeur vers un tableau de pointeurs vers des char et
     initialisé à l'adresse de t ci-dessus 
    char *(*p)[3] = &tableau;
 
  for (int i = 0; i < N; i++)
    printf("%s ", (*p)[i]);
  printf("!\n");
     */
    char *tableau[3] = {"aaaa", "bbbb", "ccccc"};
    char *tableau2[3];


    char *(*p)[3] = &tableau;
    char *(*p2)[3] = &tableau2;

    for (int i = 0; i < 3; i++)
    {
        cout << "adresse de p ? : " << (&p) << endl;
        cout << "valeur de la première case du tableau de pointeur ? : " << (*p) << endl;
        cout << "adresse la case courante : " << &(*p)[i] << endl;
        cout << "valeur de la case courante : " << (*p)[i] << endl;
        cout << "valeur du pointeur ? : " << *(*p)[i] << endl;
    }

    cout << "========================" << endl;

    (*p2)[0] = new char[4];
    strcpy((*p2)[0], "bla");

    (*p2)[1] = new char[10];
    strcpy((*p2)[1], "ababababb");

    (*p2)[2] = new char[2];
    strcpy((*p2)[2], "o");

    for (int i = 0; i < 3; i++)
    {
        cout << "adresse de p ? : " << (&p2) << endl;
        cout << "valeur de la première case du tableau de pointeur ? : " << (*p2) << endl;
        cout << "adresse la case courante : " << &(*p2)[i] << endl;

        cout << "valeur de la case courante courante : " << (*p2)[i] << endl;
        cout << "valeur du pointeur ? : " << *(*p2)[i] << endl;
    }

    cout << "========================" << endl;

    char ** Pointeur;
    char * p3[3] = {"blabla", "cacacaca", "dadadadad"};

    Pointeur = &(*p3);

    cout << Pointeur << endl;
    cout << *Pointeur << endl;
    cout << **Pointeur << endl;
    cout << &Pointeur << endl;
    cout << &*Pointeur << endl;
    cout << &**Pointeur << endl;
    cout << Pointeur << endl;


    for (int i = 0; i < 3; i++)
    {
        cout << "adresse de p ? : " << (&Pointeur) << endl;
        cout << "valeur de la première case du tableau de pointeur ? : " << (*Pointeur) << endl;
        cout << "adresse la case courante : " << &(*Pointeur)[i] << endl;
        cout << "valeur de la case courante : " << (*Pointeur)[i] << endl;
        //cout << "valeur du pointeur ? : " << *(*Pointeur)[i] << endl;
    }
    //    char *tableau3[3];
    //    char *(*p3)[];
    //
    //    *(*p3)[3] = &tableau3;
}

static void testTableauPointeur2() {
    int tableau[10] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};

    //monObjet* MonNomDeTableauD'Objet[taille];
    char* monNomDeTableau[10];

    //MonTableau[x] = new MonObjet();
    monNomDeTableau[0] = new char[4];
    strcpy(monNomDeTableau[0], "bla"); //on touche l'objet.

    cout << monNomDeTableau[0] << endl;

}

static void testTableauPointeur3() {
// NOTE : avec des INT la logique des pointeurs est correcte ! 
    
    //monObjet* MonNomDeTableauD'Objet[taille];
    int* monNomDeTableau[10]; 

    //MonTableau[x] = new MonObjet();
    monNomDeTableau[0] = new int;
    monNomDeTableau[1] = new int;
    *monNomDeTableau[0] = 2;

    cout << "Adresse de la case N°0 " << monNomDeTableau[0] << endl;
    cout << "Adresse de la case N°1 " << monNomDeTableau[1] << endl;
}

static void testTableauPointeur4() {
    int ** pointeurDeTableauDePointeurs; // équivalent à "int * pointeur[];" STRICTEMENT !
    int * pointeurDeTableau;
    
    // intialisation d'un autre tableau de pointeur
    
    int * monautrePointeurDeTableau = new int[10];
    pointeurDeTableau = monautrePointeurDeTableau;
    *pointeurDeTableauDePointeurs = monautrePointeurDeTableau;   
    monautrePointeurDeTableau[0] = 10;
    cout << monautrePointeurDeTableau[0] << endl;
    // et après ... mystère
    //pointeurDeTableauDePointeurs[0] = new int[10];
    
    
}

static void testTableauPointeur5() {
    
    int * pointeurSurTableauDeInt = new int[10];
    
    int ** pointeurDeTableauDePointeurs;

    {
        int ** pointeurDeTableauDePointeurTemporaire = new int*[10]; //Créé un tableau de pointeurs qui pointent n'importe où.
        for (int i=0; i < 10; i++)
        {
            *pointeurDeTableauDePointeurTemporaire[i] = 5; // équivalent à un "new MonObjet(param);"
            cout << pointeurDeTableauDePointeurTemporaire[i] << endl;
        }
    }
    
}


static void testTableauPointeur6() {
    
    int * pointeurSurTableauDeInt = new int[10]; // il faudra ici un int* ! 
    pointeurSurTableauDeInt[0] = 21;
    pointeurSurTableauDeInt[1] = 22;
    pointeurSurTableauDeInt[2] = 23;
    
    cout << "Adresse de la première case du tableau " << pointeurSurTableauDeInt << endl;
    cout << "Adresse de la case où est rangé la valeur de l'adresse de la première case du tableau " << &pointeurSurTableauDeInt << endl;

        
    cout << "Adresse de la première case du tableau (&[])" << &pointeurSurTableauDeInt[0] << endl;
    cout << "Valeur dans la première case du tableau " << pointeurSurTableauDeInt[0] << endl;
    
    cout << "Adresse de la deuxième case du tableau (&[])" << &pointeurSurTableauDeInt[1] << endl;
    cout << "Valeur dans la deuxième case du tableau " << pointeurSurTableauDeInt[1] << endl;
    
    int ** pointeurDeTableauDePointeurs;
    
    pointeurDeTableauDePointeurs = &pointeurSurTableauDeInt;

    cout << "Adresse de la première case du tableau (*)" << *pointeurDeTableauDePointeurs << endl;
    cout << "Adresse de la case où est rangé la valeur de l'adresse de la première case du tableau (&*)" << &*pointeurDeTableauDePointeurs << endl;
    
    cout << "Adresse de la case où est rangé l'adresse de la première case du tableau " << pointeurDeTableauDePointeurs << endl;
    cout << "Adresse de la case où est rangé la valeur de l'adresse de la case où est rangé l'adresse de la première case du tableau (&)" << &pointeurDeTableauDePointeurs << endl;

    cout << "Adresse de la première case du tableau (&*[])" << &*pointeurDeTableauDePointeurs[0] << endl;
    cout << "Valeur dans la première case du tableau (*)" << (*pointeurDeTableauDePointeurs)[0] << endl;
    
    cout << "Adresse de la deuxième case du tableau (&*[])" << &((*pointeurDeTableauDePointeurs)[1]) << endl; // IDEM ! Parenthèses obligatoires ! 
    cout << "Valeur dans la deuxième case du tableau (*)" << (*pointeurDeTableauDePointeurs)[1] << endl; // Attention ! Les parenthèses sont nécessaire ! Sinon on vient modifier pointeurDeTableauDePointeurs avant ! 
    
}

int main() {
    cout << "Coucou" << endl;
    testTableauPointeur6();

    return 0;
}


